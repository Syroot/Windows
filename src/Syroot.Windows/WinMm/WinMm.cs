﻿using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    /// <summary>
    /// Represents definitions in the Windows IpHlpApi library as part of the WinAPI.
    /// </summary>
    public static partial class WinMm
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

#pragma warning disable IDE1006 // Naming Styles, WinMm uses lowercase method names.
        /// <summary>
        /// Requests a minimum resolution for periodic timers.
        /// </summary>
        /// <param name="uPeriod">Minimum timer resolution, in milliseconds, for the application or device driver. A
        /// lower value specifies a higher (more accurate) resolution.</param>
        /// <returns>Returns <see cref="MmResult.TIMERR_NOERROR"/> if successful or
        /// <see cref="MmResult.TIMERR_NOCANDO"/> if the resolution specified in <paramref name="uPeriod"/> is out of
        /// range.</returns>
        [DllImport(nameof(WinMm))]
        public static extern MmResult timeBeginPeriod(uint uPeriod);

        /// <summary>
        /// Clears a previously set minimum timer resolution.
        /// </summary>
        /// <param name="uPeriod">Minimum timer resolution specified in the previous call to the
        /// <see cref="timeBeginPeriod"/> function.</param>
        /// <returns>Returns <see cref="MmResult.TIMERR_NOERROR"/> if successful or
        /// <see cref="MmResult.TIMERR_NOCANDO"/> if the resolution specified in <paramref name="uPeriod"/> is out of
        /// range.</returns>
        [DllImport(nameof(WinMm))]
        public static extern MmResult timeEndPeriod(uint uPeriod);

        /// <summary>
        /// Retrieves the system time, in milliseconds. The system time is the time elapsed since Windows was started.
        /// </summary>
        /// <returns>Returns the system time, in milliseconds.</returns>
        [DllImport(nameof(WinMm))]
        public static extern uint timeGetTime();
#pragma warning restore IDE1006
    }
}
