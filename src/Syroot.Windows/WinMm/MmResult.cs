﻿namespace Syroot.Windows
{
    public static partial class WinMm
    {
        /// <summary>
        /// Represents the result code returned from WinMm related functions.
        /// </summary>
        public enum MmResult
        {
            /// <summary>No error.</summary>
            TIMERR_NOERROR = 0,
            /// <summary>Request not completed.</summary>
            TIMERR_NOCANDO = 96 + 1,
            /// <summary>Time struct size.</summary>
            TIMERR_STRUCT = 96 + 33
        }
    }
}
