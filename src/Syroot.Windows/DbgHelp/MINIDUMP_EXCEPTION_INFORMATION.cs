﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    public static partial class DbgHelp
    {
        /// <summary>
        /// Contains the exception information written to the minidump file by the MiniDumpWriteDump function.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct MINIDUMP_EXCEPTION_INFORMATION
        {
            // ---- FIELDS ---------------------------------------------------------------------------------------------

            /// <summary>The identifier of the thread throwing the exception.</summary>
            public uint ThreadId;
            /// <summary>A pointer to an EXCEPTION_POINTERS structure specifying a computer-independent description of
            /// the exception and the processor context at the time of the exception.</summary>
            public IntPtr ExceptionPointers;
            /// <summary>Determines where to get the memory regions pointed to by the ExceptionPointers member.</summary>
            public bool ClientPointers;
        }
    }
}
