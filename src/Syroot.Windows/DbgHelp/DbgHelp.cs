﻿using System;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace Syroot.Windows
{
    /// <summary>
    /// Represents definitions in the Windows DbgHelp library as part of the WinAPI.
    /// </summary>
    public static partial class DbgHelp
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Writes user-mode minidump information to the specified file.
        /// </summary>
        /// <param name="hProcess">A handle to the process for which the information is to be generated.</param>
        /// <param name="processId">The identifier of the process for which the information is to be generated.</param>
        /// <param name="hFile">A handle to the file in which the information is to be written.</param>
        /// <param name="dumpType">The type of information to be generated.</param>
        /// <param name="exceptionParam">A pointer to a <see cref="MINIDUMP_EXCEPTION_INFORMATION"/> structure
        /// describing the client exception that caused the minidump to be generated.</param>
        /// <param name="userStreamParam">A pointer to a <see cref="MINIDUMP_USER_STREAM_INFORMATION"/> structure.</param>
        /// <param name="callbackParam">A pointer to a MINIDUMP_CALLBACK_INFORMATION structure that specifies a callback
        /// routine which is to receive extended minidump information.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>; otherwise, the return value
        /// is <see langword="false"/>.</returns>
        [DllImport(nameof(DbgHelp), SetLastError = true)]
        public static extern bool MiniDumpWriteDump(IntPtr hProcess, uint processId, SafeFileHandle hFile,
            MINIDUMP_TYPE dumpType, ref MINIDUMP_EXCEPTION_INFORMATION exceptionParam,
            ref MINIDUMP_USER_STREAM_INFORMATION userStreamParam, IntPtr callbackParam);

        /// <summary>
        /// Writes user-mode minidump information to the specified file.
        /// </summary>
        /// <param name="hProcess">A handle to the process for which the information is to be generated.</param>
        /// <param name="processId">The identifier of the process for which the information is to be generated.</param>
        /// <param name="hFile">A handle to the file in which the information is to be written.</param>
        /// <param name="dumpType">The type of information to be generated.</param>
        /// <param name="exceptionParam">A pointer to a <see cref="MINIDUMP_EXCEPTION_INFORMATION"/> structure
        /// describing the client exception that caused the minidump to be generated.</param>
        /// <param name="userStreamParam">A pointer to a <see cref="MINIDUMP_USER_STREAM_INFORMATION"/> structure.</param>
        /// <param name="callbackParam">A pointer to a MINIDUMP_CALLBACK_INFORMATION structure that specifies a callback
        /// routine which is to receive extended minidump information.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>; otherwise, the return value
        /// is <see langword="false"/>.</returns>
        [DllImport(nameof(DbgHelp), SetLastError = true)]
        public static extern bool MiniDumpWriteDump(IntPtr hProcess, uint processId, SafeFileHandle hFile,
            MINIDUMP_TYPE dumpType, ref MINIDUMP_EXCEPTION_INFORMATION exceptionParam, IntPtr userStreamParam,
            IntPtr callbackParam);
    }
}
