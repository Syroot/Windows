﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    public static partial class DbgHelp
    {
        /// <summary>
        /// Contains a list of user data streams used by the MiniDumpWriteDump function.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct MINIDUMP_USER_STREAM_INFORMATION
        {
            // ---- FIELDS -------------------------------------------------------------------------------------------------

            /// <summary>The number of user streams.</summary>
            public uint UserStreamCount;
            /// <summary>An array of MINIDUMP_USER_STREAM structures.</summary>
            public IntPtr UserStreamArray;
        }
    }
}
