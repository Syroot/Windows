﻿using System;

namespace Syroot.Windows
{
    public static partial class DbgHelp
    {
        /// <summary>
        /// Identifies the type of information that will be written to the minidump file by the MiniWriteDump function.
        /// </summary>
        [Flags]
        public enum MINIDUMP_TYPE
        {
            /// <summary>Include just the information necessary to capture stack traces for all existing threads in a
            /// process.</summary>
            MiniDumpNormal = 0,
            /// <summary>Include the data sections from all loaded modules.</summary>
            MiniDumpWithDataSegs = 1 << 0,
            /// <summary>Include all accessible memory in the process.</summary>
            MiniDumpWithFullMemory = 1 << 1,
            /// <summary>Include high-level information about the operating system handles that are active when the
            /// minidump is made.</summary>
            MiniDumpWithHandleData = 1 << 2,
            /// <summary>Stack and backing store memory written to the minidump file should be filtered to remove all
            /// but the pointer values necessary to reconstruct a stack trace.</summary>
            MiniDumpFilterMemory = 1 << 3,
            /// <summary>Stack and backing store memory should be scanned for pointer references to modules in the
            /// module list.</summary>
            MiniDumpScanMemory = 1 << 4,
            /// <summary>Include information from the list of modules that were recently unloaded, if this information
            /// is maintained by the operating system.</summary>
            MiniDumpWithUnloadedModules = 1 << 5,
            /// <summary>Include pages with data referenced by locals or other stack memory.</summary>
            MiniDumpWithIndirectlyReferencedMemory = 1 << 6,
            /// <summary>Filter module paths for information such as user names or important directories.</summary>
            MiniDumpFilterModulePaths = 1 << 7,
            /// <summary>Include complete per-process and per-thread information from the operating system.</summary>
            MiniDumpWithProcessThreadData = 1 << 8,
            /// <summary>Scan the virtual address space for PAGE_READWRITE memory to be included.</summary>
            MiniDumpWithPrivateReadWriteMemory = 1 << 9,
            /// <summary>Reduce the data that is dumped by eliminating memory regions that are not essential to meet
            /// criteria specified for the dump.</summary>
            MiniDumpWithoutOptionalData = 1 << 10,
            /// <summary>Include memory region information.</summary>
            MiniDumpWithFullMemoryInfo = 1 << 11,
            /// <summary>Include thread state information.</summary>
            MiniDumpWithThreadInfo = 1 << 12,
            /// <summary>Include all code and code-related sections from loaded modules to capture executable content.</summary>
            MiniDumpWithCodeSegs = 1 << 13,
            /// <summary>Turns off secondary auxiliary-supported memory gathering.</summary>
            MiniDumpWithoutAuxiliaryState = 1 << 14,
            /// <summary>Requests that auxiliary data providers include their state in the dump image; the state data
            /// that is included is provider dependent.</summary>
            MiniDumpWithFullAuxiliaryState = 1 << 15,
            /// <summary>Scans the virtual address space for PAGE_WRITECOPY memory to be included.</summary>
            MiniDumpWithPrivateWriteCopyMemory = 1 << 16,
            /// <summary>If you specify MiniDumpWithFullMemory, the MiniDumpWriteDump function will fail if the function
            /// cannot read the memory regions; however, if you include MiniDumpIgnoreInaccessibleMemory, the
            /// MiniDumpWriteDump function will ignore the memory read failures and continue to generate the dump.</summary>
            MiniDumpIgnoreInaccessibleMemory = 1 << 17,
            /// <summary>Adds security token related data.</summary>
            MiniDumpWithTokenInformation = 1 << 18,
            /// <summary>Adds module header related data.</summary>
            MiniDumpWithModuleHeaders = 1 << 19,
            /// <summary>Adds filter triage related data.</summary>
            MiniDumpFilterTriage = 1 << 20,
            MiniDumpWithAvxXStateContext = 1 << 21,
            MiniDumpWithIptTrace = 1 << 22
        }
    }
}
