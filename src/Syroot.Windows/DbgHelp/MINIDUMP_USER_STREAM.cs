﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    public static partial class DbgHelp
    {
        /// <summary>
        /// Contains user-defined information stored in a data stream.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, Pack = 4)]
        public struct MINIDUMP_USER_STREAM
        {
            // ---- FIELDS ---------------------------------------------------------------------------------------------

            /// <summary>The type of data stream.</summary>
            public MINIDUMP_STREAM_TYPE Type;
            /// <summary>The size of the user-defined data stream buffer, in bytes.</summary>
            public uint BufferSize;
            /// <summary>A pointer to a buffer that contains the user-defined data stream.</summary>
            public IntPtr Buffer;
        }
    }
}
