﻿namespace Syroot.Windows
{
    public static partial class DbgHelp
    {
        /// <summary>
        /// Represents the type of a minidump data stream.
        /// </summary>
        public enum MINIDUMP_STREAM_TYPE
        {
            /// <summary>Reserved.</summary>
            UnusedStream = 0,
            /// <summary>Reserved.</summary>
            ReservedStream0 = 1,
            /// <summary>Reserved.</summary>
            ReservedStream1 = 2,
            /// <summary>The stream contains thread information.</summary>
            ThreadListStream = 3,
            /// <summary>The stream contains module information.</summary>
            ModuleListStream = 4,
            /// <summary>The stream contains memory allocation information.</summary>
            MemoryListStream = 5,
            /// <summary>The stream contains exception information.</summary>
            ExceptionStream = 6,
            /// <summary>The stream contains general system information.</summary>
            SystemInfoStream = 7,
            /// <summary>The stream contains extended thread information.</summary>
            ThreadExListStream = 8,
            /// <summary>The stream contains memory allocation information.</summary>
            Memory64ListStream = 9,
            /// <summary>The stream contains an ANSI string used for documentation purposes.</summary>
            CommentStreamA = 10,
            /// <summary>The stream contains a Unicode string used for documentation purposes.</summary>
            CommentStreamW = 11,
            /// <summary>The stream contains high-level information about the active operating system handles.</summary>
            HandleDataStream = 12,
            /// <summary>The stream contains function table information.</summary>
            FunctionTableStream = 13,
            /// <summary>The stream contains module information for the unloaded modules.</summary>
            UnloadedModuleListStream = 14,
            /// <summary>The stream contains miscellaneous information.</summary>
            MiscInfoStream = 15,
            /// <summary>The stream contains memory region description information.</summary>
            MemoryInfoListStream = 16,
            /// <summary>The stream contains thread state information.</summary>
            ThreadInfoListStream = 17,
            /// <summary>This stream contains operation list information.</summary>
            HandleOperationListStream = 18,
            TokenStream = 19,
            JavaScriptDataStream = 20,
            SystemMemoryInfoStream = 21,
            ProcessVmCountersStream = 22,
            IptTraceStream = 23,
            ThreadNamesStream = 24,
            ceStreamNull = 0x8000,
            ceStreamSystemInfo = 0x8001,
            ceStreamException = 0x8002,
            ceStreamModuleList = 0x8003,
            ceStreamProcessList = 0x8004,
            ceStreamThreadList = 0x8005,
            ceStreamThreadContextList = 0x8006,
            ceStreamThreadCallStackList = 0x8007,
            ceStreamMemoryVirtualList = 0x8008,
            ceStreamMemoryPhysicalList = 0x8009,
            ceStreamBucketParameters = 0x800A,
            ceStreamProcessModuleMap = 0x800B,
            ceStreamDiagnosisList = 0x800C,
            /// <summary>Any value greater than this value will not be used by the system and can be used to represent
            /// application-defined data streams.</summary>
            LastReservedStream = 0xFFFF
        }
    }
}
