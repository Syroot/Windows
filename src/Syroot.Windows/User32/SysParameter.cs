﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents the system-wide parameter to be retrieved or set.
        /// </summary>
        public enum SysParameter : uint
        {
            /// <summary>Retrieves the size of the work area on the primary display monitor. The work area is the
            /// portion of the screen not obscured by the system taskbar or by application desktop toolbars.</summary>
            SPI_GETWORKAREA = 0x0030,
            /// <summary>Retrieves information about the StickyKeys accessibility feature. The pvParam parameter must
            /// point to a STICKYKEYS structure that receives the information.</summary>
            SPI_GETSTICKYKEYS = 0x003A,
            /// <summary>Sets the parameters of the StickyKeys accessibility feature. The pvParam parameter must point
            /// to a STICKYKEYS structure that contains the new parameters.</summary>
            SPI_SETSTICKYKEYS = 0x003B
        }
    }
}
