﻿using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Contains information about the StickyKeys accessibility feature.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct STICKYKEYS
        {
            // ---- FIELDS ---------------------------------------------------------------------------------------------

            /// <summary>Specifies the size, in bytes, of this structure.</summary>
            public uint cbSize;
            /// <summary>A set of bit-flags that specify properties of the StickyKeys feature.</summary>
            public StickyKeysFlags dwFlags;
        }
    }
}
