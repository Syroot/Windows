﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        public enum WindowLongQuery : int
        {
            /// <summary>Sets the user data associated with the window.</summary>
            GWL_USERDATA = -21,
            /// <summary>Sets a new extended window style.</summary>
            GWL_EXSTYLE = -20,
            /// <summary>Sets a new window style.</summary>
            GWL_STYLE = -16,
            /// <summary>Sets a new identifier of the child window.</summary>
            GWL_ID = -12,
            /// <summary>Sets a new application instance handle.</summary>
            GWL_HINSTANCE = -6,
            /// <summary>Sets a new address for the window procedure.</summary>
            GWL_WNDPROC = -4
        }
    }
}
