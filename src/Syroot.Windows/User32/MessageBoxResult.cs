﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents the result of a <see cref="MessageBox"/> call.
        /// </summary>
        public enum MessageBoxResult
        {
            /// <summary>The OK button was selected.</summary>
            IDOK = 1,
            /// <summary>The Cancel button was selected.</summary>
            IDCANCEL = 2,
            /// <summary>The Abort button was selected.</summary>
            IDABORT = 3,
            /// <summary>The Retry button was selected.</summary>
            IDRETRY = 4,
            /// <summary>The Ignore button was selected.</summary>
            IDIGNORE = 5,
            /// <summary>The Yes button was selected.</summary>
            IDYES = 6,
            /// <summary>The No button was selected.</summary>
            IDNO = 7,
            /// <summary>The Try Again button was selected.</summary>
            IDTRYAGAIN = 10,
            /// <summary>The Continue button was selected.</summary>
            IDCONTINUE = 11
        }
    }
}
