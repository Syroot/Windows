﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents <see cref="ShowWindow"/> commands.
        /// </summary>
        public enum ShowWindowCmd : int
        {
            /// <summary>Hides the window and activates another window.</summary>
            SW_HIDE = 0,
            /// <summary>Activates and displays a window. </summary>
            SW_SHOWNORMAL = 1,
            /// <summary>Activates and displays a window. </summary>
            SW_NORMAL = 1,
            /// <summary>Activates the window and displays it as a minimized window.</summary>
            SW_SHOWMINIMIZED = 2,
            /// <summary>Activates the window and displays it as a maximized window.</summary>
            SW_SHOWMAXIMIZED = 3,
            /// <summary>Maximizes the specified window.</summary>
            SW_MAXIMIZE = 3,
            /// <summary>Displays a window in its most recent size and position.</summary>
            SW_SHOWNOACTIVATE = 4,
            /// <summary>Activates the window and displays it in its current size and position.</summary>
            SW_SHOW = 5,
            /// <summary>Minimizes the specified window and activates the next top-level window in the Z order.</summary>
            SW_MINIMIZE = 6,
            /// <summary>Displays the window as a minimized window.</summary>
            SW_SHOWMINNOACTIVE = 7,
            /// <summary>Displays the window in its current size and position.</summary>
            SW_SHOWNA = 8,
            /// <summary>Activates and displays the window.</summary>
            SW_RESTORE = 9,
            /// <summary>Sets the show state based on the SW_ value specified in the STARTUPINFO structure passed to the
            /// CreateProcess function by the program that started the application.</summary>
            SW_SHOWDEFAULT = 10,
            /// <summary>Minimizes a window, even if the thread that owns the window is not responding.</summary>
            SW_FORCEMINIMIZE = 11
        }
    }
}
