﻿using System;

namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents extended window styles.
        /// </summary>
        [Flags]
        public enum WindowStyleExtended : uint
        {
            /// <summary>The window has generic left-aligned properties. This is the default.</summary>
            WS_EX_LEFT = 0,
            /// <summary>The window text is displayed using left-to-right reading-order properties. This is the default.</summary>
            WS_EX_LTRREADING = 0,
            /// <summary>The vertical scroll bar (if present) is to the right of the client area. This is the default.</summary>
            WS_EX_RIGHTSCROLLBAR = 0,
            /// <summary>The window has a double border.</summary>
            WS_EX_DLGMODALFRAME = 1 << 0,
            /// <summary>The child window created with this style does not send the WM_PARENTNOTIFY message to its
            /// parent window when it is created or destroyed.</summary>
            WS_EX_NOPARENTNOTIFY = 1 << 2,
            /// <summary>The window should be placed above all non-topmost windows and should stay above them, even when
            /// the window is deactivated.</summary>
            WS_EX_TOPMOST = 1 << 3,
            /// <summary>The window accepts drag-drop files.</summary>
            WS_EX_ACCEPTFILES = 1 << 4,
            /// <summary>The window should not be painted until siblings beneath the window (that were created by the
            /// same thread) have been painted.</summary>
            WS_EX_TRANSPARENT = 1 << 5,
            /// <summary>The window is a MDI child window.</summary>
            WS_EX_MDICHILD = 1 << 6,
            /// <summary>The window is intended to be used as a floating toolbar.</summary>
            WS_EX_TOOLWINDOW = 1 << 7,
            /// <summary>The window has a border with a raised edge.</summary>
            WS_EX_WINDOWEDGE = 1 << 8,
            /// <summary>The window has a border with a sunken edge.</summary>
            WS_EX_CLIENTEDGE = 1 << 9,
            /// <summary>The title bar of the window includes a question mark.</summary>
            WS_EX_CONTEXTHELP = 1 << 10,
            /// <summary>The window has generic "right-aligned" properties.</summary>
            WS_EX_RIGHT = 1 << 12,
            /// <summary>If the shell language is Hebrew, Arabic, or another language that supports reading-order
            /// alignment, the window text is displayed using right-to-left reading-order properties.</summary>
            WS_EX_RTLREADING = 1 << 13,
            /// <summary>If the shell language is Hebrew, Arabic, or another language that supports reading order
            /// alignment, the vertical scroll bar (if present) is to the left of the client area.</summary>
            WS_EX_LEFTSCROLLBAR = 1 << 14,
            /// <summary>The window itself contains child windows that should take part in dialog box navigation.</summary>
            WS_EX_CONTROLPARENT = 1 << 16,
            /// <summary>The window has a three-dimensional border style intended to be used for items that do not
            /// accept user input.</summary>
            WS_EX_STATICEDGE = 1 << 17,
            /// <summary>Forces a top-level window onto the taskbar when the window is visible.</summary>
            WS_EX_APPWINDOW = 1 << 18,
            /// <summary>The window is a layered window.</summary>
            WS_EX_LAYERED = 1 << 19,
            /// <summary>The window does not pass its window layout to its child windows.</summary>
            WS_EX_NOINHERITLAYOUT = 1 << 20,
            /// <summary>The window does not render to a redirection surface.</summary>
            WS_EX_NOREDIRECTIONBITMAP = 1 << 21,
            /// <summary>If the shell language is Hebrew, Arabic, or another language that supports reading order
            /// alignment, the horizontal origin of the window is on the right edge.</summary>
            WS_EX_LAYOUTRTL = 1 << 22,
            /// <summary>Paints all descendants of a window in bottom-to-top painting order using double-buffering.</summary>
            WS_EX_COMPOSITED = 1 << 25,
            /// <summary>A top-level window created with this style does not become the foreground window when the user
            /// clicks it.</summary>
            WS_EX_NOACTIVATE = 1 << 27,
            /// <summary>The window is an overlapped window.</summary>
            WS_EX_OVERLAPPEDWINDOW = WS_EX_WINDOWEDGE | WS_EX_CLIENTEDGE,
            /// <summary>The window is palette window, which is a modeless dialog box that presents an array of
            /// commands.</summary>
            WS_EX_PALETTEWINDOW = WS_EX_WINDOWEDGE | WS_EX_TOOLWINDOW | WS_EX_TOPMOST
        }
    }
}
