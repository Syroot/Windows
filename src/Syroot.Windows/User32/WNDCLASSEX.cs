﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Contains window class information.
        /// </summary>
        [StructLayout(LayoutKind.Sequential, CharSet = CharSet.Auto)]
        public struct WNDCLASSEX
        {
            /// <summary>The size, in bytes, of this structure.</summary>
            public uint cbSize;
            /// <summary>The class style(s).</summary>
            public ClassStyle style;
            /// <summary>A pointer to the window procedure.</summary>
            public IntPtr lpfnWndProc;
            /// <summary>The number of extra bytes to allocate following the window-class structure.</summary>
            public int cbClsExtra;
            /// <summary>The number of extra bytes to allocate following the window instance.</summary>
            public int cbWndExtra;
            /// <summary>A handle to the instance that contains the window procedure for the class.</summary>
            public HINSTANCE hInstance;
            /// <summary>A handle to the class icon.</summary>
            public HICON hIcon;
            /// <summary>A handle to the class cursor.</summary>
            public HCURSOR hCursor;
            /// <summary>A handle to the class background brush.</summary>
            public HBRUSH hbrBackground;
            /// <summary>Pointer to a null-terminated character string that specifies the resource name of the class
            /// menu, as the name appears in the resource file.
            /// </summary>
            public string lpszMenuName;
            /// <summary>A pointer to a null-terminated string or is an atom.</summary>
            public string lpszClassName;
            /// <summary>A handle to a small icon that is associated with the window class.</summary>
            public HICON hIconSm;
        }
    }
}
