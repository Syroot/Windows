﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents a system command.
        /// </summary>
        public enum SysCommand : uint
        {
            /// <summary>Moves to the next window.</summary>
            SC_NEXTWINDOW = 0xF040,
            /// <summary>Moves to the previous window.</summary>
            SC_PREVWINDOW = 0xF050,
            /// <summary>Retrieves the window menu as a result of a mouse click.
            /// </summary>
            SC_MOUSEMENU = 0xF090,
            /// <summary>Retrieves the window menu as a result of a keystroke.
            /// </summary>
            SC_KEYMENU = 0xF100
        }
    }
}
