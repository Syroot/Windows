﻿using System.Drawing;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents the coordinates of the upper-left and lower-right corners of a rectangle.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct RECT
        {
            // ---- FIELDS ---------------------------------------------------------------------------------------------

            /// <summary>The x-coordinate of the upper-left corner of the rectangle.</summary>
            public int left;
            /// <summary>The y-coordinate of the upper-left corner of the rectangle.</summary>
            public int top;
            /// <summary>The x-coordinate of the lower-right corner of the rectangle.</summary>
            public int right;
            /// <summary>The y-coordinate of the lower-right corner of the rectangle.</summary>
            public int bottom;

            // ---- CONSTRUCTORS & DESTRUCTOR --------------------------------------------------------------------------

            /// <summary>
            /// Initializes a new instance of the <see cref="RECT"/> struct with the given dimensions.
            /// </summary>
            /// <param name="left">The x-coordinate of the upper-left corner of the rectangle.</param>
            /// <param name="top">The y-coordinate of the upper-left corner of the rectangle.</param>
            /// <param name="right">The x-coordinate of the lower-right corner of the rectangle.</param>
            /// <param name="bottom">The y-coordinate of the lower-right corner of the rectangle.</param>
            public RECT(int left, int top, int right, int bottom)
            {
                this.left = left;
                this.top = top;
                this.right = right;
                this.bottom = bottom;
            }

            // ---- PROPERTIES -----------------------------------------------------------------------------------------

            /// <summary>
            /// Gets the width of the rectangle.
            /// </summary>
            public int Width => right - left;

            /// <summary>
            /// Gets the height of the rectangle.
            /// </summary>
            public int Height => bottom - top;

            // ---- OPERATORS ------------------------------------------------------------------------------------------

            /// <summary>
            /// Implicitly converts a <see cref="Rectangle"/> to a <see cref="RECT"/> instance.
            /// </summary>
            /// <param name="rectangle">The <see cref="Rectangle"/> instance to convert.</param>
            public static implicit operator RECT(Rectangle rectangle)
                => new RECT(rectangle.Left, rectangle.Top, rectangle.Width, rectangle.Height);

            /// <summary>
            /// Implicitly converts a <see cref="RECT"/> to a <see cref="Rectangle"/> instance.
            /// </summary>
            /// <param name="rect">The <see cref="RECT"/> instance to convert.</param>
            public static implicit operator Rectangle(RECT rect)
                => new Rectangle(rect.left, rect.top, rect.Width, rect.Height);
        }
    }
}
