﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents how messages are to be handled in <see cref="User32.PeekMessage"/>.
        /// </summary>
        public enum PeekMessageRemove : uint
        {
            /// <summary>Messages are not removed from the queue after processing by PeekMessage.</summary>
            PM_NOREMOVE,
            /// <summary>Messages are removed from the queue after processing by PeekMessage.</summary>
            PM_REMOVE,
            /// <summary>Prevents the system from releasing any thread that is waiting for the caller to go idle.</summary>
            PM_NOYIELD
        }
    }
}
