﻿using System.Runtime.InteropServices;
using static Syroot.Windows.Gdi32;

namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// The PAINTSTRUCT structure contains information for an application. This information can be used to paint the
        /// client area of a window owned by that application.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct PAINTSTRUCT
        {
            /// <summary>A handle to the display DC to be used for painting.</summary>
            public HDC hdc;
            /// <summary>Indicates whether the background must be erased.</summary>
            public bool fErase;
            /// <summary>A <see cref="RECT"/> structure that specifies the upper left and lower right corners of the
            /// rectangle in which the painting is requested, in device units relative to the upper-left corner of the
            /// client area.</summary>
            public RECT rcPaint;
            /// <summary>Reserved; used internally by the system.</summary>
            public bool fRestore;
            /// <summary>Reserved; used internally by the system.</summary>
            public bool fIncUpdate;
            /// <summary>Reserved; used internally by the system.</summary>
            [MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
            public byte[] rgbReserved;
        }
    }
}
