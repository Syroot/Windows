﻿using System;

namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents the window sizing and positioning flags.
        /// </summary>
        [Flags]
        public enum SetWindowPosFlags : uint
        {
            SWP_NOSIZE = 1 << 0,
            SWP_NOMOVE = 1 << 1,
            SWP_NOZORDER = 1 << 2,
            SWP_NOREDRAW = 1 << 3,
            SWP_NOACTIVATE = 1 << 4,
            SWP_FRAMECHANGED = 1 << 5,
            SWP_SHOWWINDOW = 1 << 6,
            SWP_HIDEWINDOW = 1 << 7,
            SWP_NOCOPYBITS = 1 << 8,
            SWP_NOOWNERZORDER = 1 << 9,
            SWP_NOSENDCHANGING = 1 << 10,
            SWP_DRAWFRAME = SWP_FRAMECHANGED,
            SWP_NOREPOSITION = SWP_NOOWNERZORDER,
            SWP_DEFERERASE = 1 << 13,
            SWP_ASYNCWINDOWPOS = 1 << 14
        }
    }
}
