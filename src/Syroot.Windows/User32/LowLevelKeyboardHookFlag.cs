﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents low level keyboard hook flags.
        /// </summary>
        public enum LowLevelKeyboardHookFlag : uint
        {
            LLKHF_EXTENDED = 0x00000001,
            LLKHF_INJECTED = 0x00000010,
            LLKHF_ALTDOWN = 0x00000020,
            LLKHF_UP = 0x00000080
        }
    }
}
