﻿using System;

namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents window class styles.
        /// </summary>
        [Flags]
        public enum ClassStyle : uint
        {
            /// <summary>Redraws the entire window if a movement or size adjustment changes the height of the client
            /// area.</summary>
            CS_VREDRAW = 1 << 0,
            /// <summary>Redraws the entire window if a movement or size adjustment changes the width of the client
            /// area.</summary>
            CS_HREDRAW = 1 << 1,
            /// <summary>Sends a double-click message to the window procedure when the user double-clicks the mouse
            /// while the cursor is within a window belonging to the class.</summary>
            CS_DBLCLKS = 1 << 3,
            /// <summary>Allocates a unique device context for each window in the class.</summary>
            CS_OWNDC = 1 << 5,
            /// <summary>Allocates one device context to be shared by all windows in the class.</summary>
            CS_CLASSDC = 1 << 6,
            /// <summary>Sets the clipping rectangle of the child window to that of the parent window so that the child
            /// can draw on the parent.
            /// </summary>
            CS_PARENTDC = 1 << 7,
            /// <summary>Disables Close on the window menu.</summary>
            CS_NOCLOSE = 1 << 9,
            /// <summary>Saves, as a bitmap, the portion of the screen image obscured by a window of this class.</summary>
            CS_SAVEBITS = 1 << 11,
            /// <summary>Aligns the window's client area on a byte boundary (in the x direction).</summary>
            CS_BYTEALIGNCLIENT = 1 << 12,
            /// <summary>Aligns the window on a byte boundary (in the x direction).</summary>
            CS_BYTEALIGNWINDOW = 1 << 13,
            /// <summary>Indicates that the window class is an application global class.</summary>
            CS_GLOBALCLASS = 1 << 14,
            CS_IME = 1 << 16,
            /// <summary>Enables the drop shadow effect on a window.</summary>
            CS_DROPSHADOW = 1 << 17
        }
    }
}
