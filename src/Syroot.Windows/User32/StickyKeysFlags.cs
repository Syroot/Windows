﻿using System;

namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents bit-flags that specify properties of the StickyKeys feature.
        /// </summary>
        [Flags]
        public enum StickyKeysFlags
        {
            /// <summary>If this flag is set, the StickyKeys feature is available.</summary>
            SKF_AVAILABLE = 0x00000002,
            /// <summary>If this flag is set, the user can turn the StickyKeys feature on and off by pressing the SHIFT
            /// key five times.
            /// </summary>
            SKF_HOTKEYACTIVE = 0x00000004,
            /// <summary>A confirmation dialog appears when the StickyKeys feature is activated by using the hot key.
            /// </summary>
            SKF_CONFIRMHOTKEY = 0x00000008,
            /// <summary>If this flag is set, the system plays a siren sound when the user turns the StickyKeys feature
            /// on or off by using the hot key.</summary>
            SKF_HOTKEYSOUND = 0x00000010,
            /// <summary>A visual indicator should be displayed when the StickyKeys feature is on.</summary>
            SKF_INDICATOR = 0x00000020,
            /// <summary>If this flag is set, the system plays a sound when the user latches, locks, or releases
            /// modifier keys using the StickyKeys feature.</summary>
            SKF_AUDIBLEFEEDBACK = 0x00000040,
            /// <summary>If this flag is set, pressing a modifier key twice in a row locks down the key until the user
            /// presses it a third time.</summary>
            SKF_TRISTATE = 0x00000080,
            /// <summary>If this flag is set, releasing a modifier key that has been pressed in combination with any
            /// other key turns off the StickyKeys feature.</summary>
            SKF_TWOKEYSOFF = 0x00000100
        }
    }
}
