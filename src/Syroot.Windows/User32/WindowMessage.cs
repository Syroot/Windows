﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents messages sent to the window loop as defined by Windows.
        /// </summary>
        public enum WindowMessage : uint
        {
            /// <summary>Performs no operation.</summary>
            WM_NULL = 0x0000,
            /// <summary>Sent when an application requests that a window be created by calling the CreateWindowEx or
            /// CreateWindow function.</summary>
            WM_CREATE = 0x0001,
            /// <summary>Sent when a window is being destroyed.</summary>
            WM_DESTROY = 0x0002,
            /// <summary>Sent after a window has been moved.</summary>
            WM_MOVE = 0x0003,
            /// <summary>Sent to a window after its size has changed.</summary>
            WM_SIZE = 0x0005,
            /// <summary>Sent to both the window being activated and the window being deactivated.</summary>
            WM_ACTIVATE = 0x0006,
            /// <summary>Sent to a window after it has gained the keyboard focus.</summary>
            WM_SETFOCUS = 0x0007,
            /// <summary>Sent to a window immediately before it loses the keyboard focus.</summary>
            WM_KILLFOCUS = 0x0008,
            /// <summary>Sent when an application changes the enabled state of a window.</summary>
            WM_ENABLE = 0x000A,
            /// <summary>Sent to a window to allow changes in that window to be redrawn or to prevent changes in that
            /// window from being redrawn.</summary>
            WM_SETREDRAW = 0x000B,
            /// <summary>Sets the text of a window.</summary>
            WM_SETTEXT = 0x000C,
            /// <summary>Copies the text that corresponds to a window into a buffer provided by the caller.</summary>
            WM_GETTEXT = 0x000D,
            /// <summary>Determines the length, in characters, of the text associated with a window.</summary>
            WM_GETTEXTLENGTH = 0x000E,
            /// <summary>Sent when the system or another application makes a request to paint a portion of an
            /// application's window.</summary>
            WM_PAINT = 0x000F,
            /// <summary>Sent as a signal that a window or an application should terminate.</summary>
            WM_CLOSE = 0x0010,
            /// <summary>Indicates a request to terminate an application, and is generated when the application calls
            /// the PostQuitMessage function.
            /// </summary>
            WM_QUIT = 0x0012,
            /// <summary>Sent when a window belonging to a different application than the active window is about to be
            /// activated.</summary>
            WM_ACTIVATEAPP = 0x001C,
            /// <summary>Sent to a window if the mouse causes the cursor to move within a window and mouse input is not
            /// captured.</summary>
            WM_SETCURSOR = 0x0020,
            /// <summary>Sent to the topmost affected window after an application's input language has been changed.</summary>
            WM_INPUTLANGCHANGE = 0x0051,
            /// <summary>Sent to all windows when the display resolution has changed.</summary>
            WM_DISPLAYCHANGE = 0x007E,
            /// <summary>Posted when the user presses the left mouse button while the cursor is within the nonclient
            /// area of a window.</summary>
            WM_NCLBUTTONDOWN = 0x00A1,
            /// <summary>Posted to the window with the keyboard focus when a nonsystem key is pressed.</summary>
            WM_KEYDOWN = 0x0100,
            /// <summary>Posted to the window with the keyboard focus when a nonsystem key is released.</summary>
            WM_KEYUP = 0x0101,
            /// <summary>Posted to the window with the keyboard focus when a WM_KEYDOWN message is translated by the
            /// TranslateMessage function. The WM_CHAR message contains the character code of the key that was pressed.</summary>
            WM_CHAR = 0x0102,
            /// <summary>Sent immediately before the IME generates the composition string as a result of a keystroke.</summary>
            WM_IME_STARTCOMPOSITION = 0x10D,
            /// <summary>Sent to an application when the IME ends composition.</summary>
            WM_IME_ENDCOMPOSITION = 0x10E,
            /// <summary>Sent to an application when the IME changes composition status as a result of a keystroke.</summary>
            WM_IME_COMPOSITION = 0x10F,
            /// <summary>A window receives this message when the user chooses a command from the Window menu or when the
            /// user chooses the maximize button, minimize button, restore button, or close button.</summary>
            WM_SYSCOMMAND = 0x0112,
            /// <summary>Posted to a window when the cursor moves.</summary>
            WM_MOUSEMOVE = 0x0200,
            /// <summary>Posted when the user presses the left mouse button while the cursor is in the client area of a
            /// window.</summary>
            WM_LBUTTONDOWN = 0x0201,
            /// <summary>Posted when the user releases the left mouse button while the cursor is in the client area of a
            /// window.</summary>
            WM_LBUTTONUP = 0x0202,
            /// <summary>Posted when the user double-clicks the left mouse button while the cursor is in the client area
            /// of a window.</summary>
            WM_LBUTTONDBLCLK = 0x0203,
            /// <summary>Posted when the user presses the right mouse button while the cursor is in the client area of a
            /// window.</summary>
            WM_RBUTTONDOWN = 0x0204,
            /// <summary>Posted when the user releases the right mouse button while the cursor is in the client area of
            /// a window.</summary>
            WM_RBUTTONUP = 0x0205,
            /// <summary>Posted when the user double-clicks the right mouse button while the cursor is in the client
            /// area of a window.</summary>
            WM_RBUTTONDBLCLK = 0x0206,
            /// <summary>Posted when the user presses the middle mouse button while the cursor is in the client area of
            /// a window.</summary>
            WM_MBUTTONDOWN = 0x0207,
            /// <summary>Posted when the user releases the middle mouse button while the cursor is in the client area of
            /// a window.</summary>
            WM_MBUTTONUP = 0x0208,
            /// <summary>Sent to the focus window when the mouse wheel is rotated.</summary>
            WM_MOUSEWHEEL = 0x020A,
            /// <summary>Sent to an application when a window is activated.</summary>
            WM_IME_SETCONTEXT = 0x281,
            /// <summary>Sent to an application to notify it of changes to the IME window.</summary>
            WM_IME_NOTIFY = 0x282,
            /// <summary>Sent to an application when the IME gets a character of the conversion result.</summary>
            WM_IME_CHAR = 0x286
        }
    }
}
