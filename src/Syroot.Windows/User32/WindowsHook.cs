﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents the type of hook procedure to be installed.
        /// </summary>
        public enum WindowsHook : uint
        {
            /// <summary>Installs a hook procedure that monitors low-level keyboard input events.</summary>
            WH_KEYBOARD_LL = 13
        }
    }
}
