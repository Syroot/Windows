﻿using System;

namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents the contents and behavior of a <see cref="MessageBox"/>.
        /// </summary>
        [Flags]
        public enum MessageBoxType
        {
            /// <summary>The message box contains one push button: OK.</summary>
            MB_OK = 0x0,
            /// <summary>The message box contains two push buttons: OK and Cancel.</summary>
            MB_OKCANCEL = 0x1,
            /// <summary>The message box contains three push buttons: Abort, Retry, and Ignore.</summary>
            MB_ABORTRETRYIGNORE = 0x2,
            /// <summary>The message box contains three push buttons: Yes, No, and Cancel.</summary>
            MB_YESNOCANCEL = 0x3,
            /// <summary>The message box contains two push buttons: Yes and No.</summary>
            MB_YESNO = 0x4,
            /// <summary>The message box contains two push buttons: Retry and Cancel.</summary>
            MB_RETRYCANCEL = 0x5,
            /// <summary>The message box contains three push buttons: Cancel, Try Again, Continue.</summary>
            MB_CANCELTRYCONTINUE = 0x6,
            /// <summary>Adds a Help button to the message box.</summary>
            MB_HELP = 0x4000,
            /// <summary>An exclamation-point icon appears in the message box.</summary>
            MB_ICONEXCLAMATION = 0x30,
            /// <summary>An exclamation-point icon appears in the message box.</summary>
            MB_ICONWARNING = 0x30,
            /// <summary>An icon consisting of a lowercase letter i in a circle appears in the message box.</summary>
            MB_ICONINFORMATION = 0x40,
            /// <summary>An icon consisting of a lowercase letter i in a circle appears in the message box.</summary>
            MB_ICONASTERISK = 0x40,
            /// <summary>A question-mark icon appears in the message box.</summary>
            MB_ICONQUESTION = 0x20,
            /// <summary>A stop-sign icon appears in the message box.</summary>
            MB_ICONSTOP = 0x10,
            /// <summary>A stop-sign icon appears in the message box.</summary>
            MB_ICONERROR = 0x10,
            /// <summary>A stop-sign icon appears in the message box.</summary>
            MB_ICONHAND = 0x10,
            /// <summary>The first button is the default button.</summary>
            MB_DEFBUTTON1 = 0,
            /// <summary>The second button is the default button.</summary>
            MB_DEFBUTTON2 = 0x100,
            /// <summary>The third button is the default button.</summary>
            MB_DEFBUTTON3 = 0x200,
            /// <summary>The fourth button is the default button.</summary>
            MB_DEFBUTTON4 = 0x300,
            /// <summary>The user must respond to the message box before continuing work in the window identified by the
            /// hWnd parameter.</summary>
            MB_APPLMODAL = 0x0,
            /// <summary>Same as MB_APPLMODAL except that the message box has the WS_EX_TOPMOST style.</summary>
            MB_SYSTEMMODAL = 0x1000,
            /// <summary>Same as MB_APPLMODAL except that all the top-level windows belonging to the current thread are
            /// disabled if the hWnd parameter is NULL.</summary>
            MB_TASKMODAL = 0x2000,
            /// <summary>Same as desktop of the interactive window station.</summary>
            MB_DEFAULT_DESKTOP_ONLY = 0x2000,
            /// <summary>The text is right-justified.</summary>
            MB_RIGHT = 0x8000,
            /// <summary>Displays message and caption text using right-to-left reading order on Hebrew and Arabic
            /// systems.</summary>
            MB_RTLREADING = 0x100000,
            /// <summary>The message box becomes the foreground window.</summary>
            MB_SETFOREGROUND = 0x10000,
            /// <summary>The message box is created with the WS_EX_TOPMOST window style.</summary>
            MB_TOPMOST = 0x40000,
            /// <summary>The caller is a service notifying the user of an event.</summary>
            MB_SERVICE_NOTIFICATION = 0x200000
        }
    }
}
