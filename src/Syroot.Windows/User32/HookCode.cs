﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// A code the hook procedure uses to determine how to process the message.
        /// </summary>
        public enum HookCode : int
        {
            /// <summary>The wParam and lParam parameters contain information about a keyboard message.</summary>
            HC_ACTION
        }
    }
}
