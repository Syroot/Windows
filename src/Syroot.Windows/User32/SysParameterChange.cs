﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Specifies whether the user profile is to be updated, and if so, whether the WM_SETTINGCHANGE message is to
        /// be broadcast to all top-level windows to notify them of the change.
        /// </summary>
        public enum SysParameterChange : uint
        {
            /// <summary>Do not update the user profile or broadcast the WM_SETTINGCHANGE message.</summary>
            None = 0,
            /// <summary>Writes the new system-wide parameter setting to the user profile.</summary>
            SPIF_UPDATEINIFILE = 1,
            /// <summary>Broadcasts the WM_SETTINGCHANGE message after updating the user profile.</summary>
            SPIF_SENDCHANGE = 2,
            /// <summary>Same as <see cref="SPIF_SENDCHANGE"/>.</summary>
            SPIF_SENDWININICHANGE = SPIF_SENDCHANGE
        }
    }
}
