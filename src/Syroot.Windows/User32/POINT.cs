﻿using System.Drawing;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents the x- and y- coordinates of a point.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct POINT
        {
            // ---- FIELDS ---------------------------------------------------------------------------------------------

            /// <summary>The x-coordinate of the point.</summary>
            public int x;
            /// <summary>The y-coordinate of the point.</summary>
            public int y;

            // ---- CONSTRUCTORS & DESTRUCTOR --------------------------------------------------------------------------

            /// <summary>
            /// Initializes a new instance of the <see cref="POINT"/> struct with the given coordinates.
            /// </summary>
            /// <param name="x">The x-coordinate of the point.</param>
            /// <param name="y">The y-coordinate of the point.</param>
            public POINT(int x, int y)
            {
                this.x = x;
                this.y = y;
            }

            // ---- OPERATORS ------------------------------------------------------------------------------------------

            /// <summary>
            /// Implicitly converts a <see cref="Point"/> to a <see cref="POINT"/> instance.
            /// </summary>
            /// <param name="point">The <see cref="Point"/> instance to convert.</param>
            public static implicit operator POINT(Point point) => new POINT(point.X, point.Y);

            /// <summary>
            /// Implicitly converts a <see cref="POINT"/> to a <see cref="Point"/> instance.
            /// </summary>
            /// <param name="point">The <see cref="POINT"/> instance to convert.</param>
            public static implicit operator Point(POINT point) => new Point(point.x, point.y);
        }
    }
}
