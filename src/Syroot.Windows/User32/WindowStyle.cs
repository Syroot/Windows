﻿using System;

namespace Syroot.Windows
{
    public static partial class User32
    {
        [Flags]
        public enum WindowStyle : uint
        {
            /// <summary>The window is an overlapped window.</summary>
            WS_OVERLAPPED = 0,
            /// <summary>The window is an overlapped window.</summary>
            WS_TILED = WS_OVERLAPPED,
            /// <summary>The window is a control that can receive the keyboard focus when the user presses the TAB key.</summary>
            WS_TABSTOP = 1 << 16,
            /// <summary>The window has a maximize button.</summary>
            WS_MAXIMIZEBOX = 1 << 16,
            /// <summary>The window is the first control of a group of controls.</summary>
            WS_GROUP = 1 << 17,
            /// <summary>The window has a minimize button.</summary>
            WS_MINIMIZEBOX = 1 << 17,
            /// <summary>The window has a sizing border.</summary>
            WS_THICKFRAME = 1 << 18,
            /// <summary>The window has a window menu on its title bar.</summary>
            WS_SYSMENU = 1 << 19,
            /// <summary>The window has a sizing border.</summary>
            WS_SIZEBOX = WS_THICKFRAME,
            /// <summary>The window has a horizontal scroll bar.</summary>
            WS_HSCROLL = 1 << 20,
            /// <summary>The window has a vertical scroll bar.</summary>
            WS_VSCROLL = 1 << 21,
            /// <summary>The window has a border of a style typically used with dialog boxes. </summary>
            WS_DLGFRAME = 1 << 22,
            /// <summary>The window has a thin-line border.</summary>
            WS_BORDER = 1 << 23,
            /// <summary>The window has a title bar.</summary>
            WS_CAPTION = 1 << 22 | 1 << 23,
            /// <summary>The window is initially maximized.</summary>
            WS_MAXIMIZE = 1 << 24,
            /// <summary>Excludes the area occupied by child windows when drawing occurs within the parent window.</summary>
            WS_CLIPCHILDREN = 1 << 25,
            /// <summary>Clips child windows relative to each other.</summary>
            WS_CLIPSIBLINGS = 1 << 26,
            /// <summary>The window is initially disabled.</summary>
            WS_DISABLED = 1 << 27,
            /// <summary>The window is initially visible.</summary>
            WS_VISIBLE = 1 << 28,
            /// <summary>The window is initially minimized.</summary>
            WS_MINIMIZE = 1 << 29,
            /// <summary>The window is initially minimized.</summary>
            WS_ICONIC = WS_MINIMIZE,
            /// <summary>The window is a child window.</summary>
            WS_CHILD = 1 << 30,
            /// <summary>The window is a child window.</summary>
            WS_CHILDWINDOW = WS_CHILD,
            /// <summary>The window is a pop-up window</summary>
            WS_POPUP = 1u << 31,
            /// <summary>The window is an overlapped window.</summary>
            WS_OVERLAPPEDWINDOW = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX,
            /// <summary>The window is an overlapped window.</summary>
            WS_TILEDWINDOW = WS_OVERLAPPEDWINDOW,
            /// <summary>The window is a pop-up window.</summary>
            WS_POPUPWINDOW = WS_POPUP | WS_BORDER | WS_SYSMENU,
        }
    }
}
