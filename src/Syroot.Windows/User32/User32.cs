﻿using System;
using System.Runtime.InteropServices;
using static Syroot.Windows.Gdi32;

namespace Syroot.Windows
{
    /// <summary>
    /// Represents definitions in the Windows User32 library as part of the WinAPI.
    /// </summary>
    public static partial class User32
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        public const int CW_USEDEFAULT = unchecked((int)0x80000000);

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Calculates the required size of the window rectangle, based on the desired client-rectangle size.
        /// </summary>
        /// <param name="lpRect">A pointer to a RECT structure that contains the coordinates of the top-left and
        /// bottom-right corners of the desired client area.</param>
        /// <param name="dwStyle">The window style of the window whose required size is to be calculated.</param>
        /// <param name="bMenu">Indicates whether the window has a menu.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool AdjustWindowRect(ref RECT lpRect, WindowStyle dwStyle, bool bMenu);

        /// <summary>
        /// Prepares the specified window for painting and fills a <see cref="PAINTSTRUCT"/> structure with information
        /// about the painting.
        /// </summary>
        /// <param name="hWnd">Handle to the window to be repainted.</param>
        /// <param name="lpPaint">Pointer to the <see cref="PAINTSTRUCT"/> structure that will receive painting
        /// information.</param>
        /// <returns>If the function succeeds, the return value is the handle to a display device context for the
        /// specified window. If the function fails, the return value is <see cref="IntPtr.Zero"/>, indicating that no
        /// display device context is available.</returns>
        [DllImport(nameof(User32))]
        public static extern HDC BeginPaint(HWND hWnd, out PAINTSTRUCT lpPaint);

        /// <summary>
        /// Passes the hook information to the next hook procedure in the current hook chain
        /// </summary>
        /// <param name="hhk">This parameter is ignored.</param>
        /// <param name="nCode">The hook code passed to the current hook procedure.</param>
        /// <param name="wParam">The wParam value passed to the current hook procedure.</param>
        /// <param name="lParam">The lParam value passed to the current hook procedure.</param>
        /// <returns>This value is returned by the next hook procedure in the chain.</returns>
        [DllImport(nameof(User32))]
        public static extern IntPtr CallNextHookEx(HHOOK hhk, HookCode nCode, ref WindowMessage wParam,
            ref KBDLLHOOKSTRUCT lParam);

        /// <summary>
        /// Passes message information to the specified window procedure.
        /// </summary>
        /// <param name="lpPrevWndFunc">The previous window procedure. If this value is obtained by calling the
        /// GetWindowLong function with the nIndex parameter set to GWL_WNDPROC or DWL_DLGPROC, it is actually either
        /// the address of a window or dialog box procedure, or a special internal value meaningful only to
        /// CallWindowProc.</param>
        /// <param name="hWnd">A handle to the window whose window procedure will receive the message.</param>
        /// <param name="msg">The message to be sent.</param>
        /// <param name="wParam">Additional message-specific information.</param>
        /// <param name="lParam">Additional message-specific information.</param>
        /// <returns>The return value specifies the result of the message processing; it depends on the message sent.</returns>
        [DllImport(nameof(User32))]
        public static extern IntPtr CallWindowProc(IntPtr lpPrevWndFunc, HWND hWnd, uint msg, IntPtr wParam,
            IntPtr lParam);

        /// <summary>
        /// Closes the clipboard.
        /// </summary>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool CloseClipboard();

        /// <summary>
        /// Creates an overlapped, pop-up, or child window.
        /// </summary>
        /// <param name="lpClassName">A null-terminated string or a class atom created by a previous call to the
        /// RegisterClass or <see cref="RegisterClassEx"/> function.</param>
        /// <param name="lpWindowName">The window name.</param>
        /// <param name="dwStyle">The style of the window being created.</param>
        /// <param name="x">The initial horizontal position of the window.</param>
        /// <param name="y">The initial vertical position of the window.</param>
        /// <param name="nWidth">The width, in device units, of the window.</param>
        /// <param name="nHeight">The height, in device units, of the window.</param>
        /// <param name="hWndParent">A handle to the parent or owner window of the window being created.</param>
        /// <param name="hMenu">A handle to a menu, or specifies a child-window identifier, depending on the window
        /// style.</param>
        /// <param name="hInstance">A handle to the instance of the module to be associated with the window.</param>
        /// <param name="lpParam">Pointer to a value to be passed to the window through the CREATESTRUCT structure
        /// (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.</param>
        public static void CreateWindow(string lpClassName, string lpWindowName, WindowStyle dwStyle, int x, int y,
            int nWidth, int nHeight, HWND hWndParent, HMENU hMenu, HINSTANCE hInstance, IntPtr lpParam)
        {
            CreateWindowEx(0, lpClassName, lpWindowName, dwStyle, x, y, nWidth, nHeight, hWndParent, hMenu, hInstance,
                lpParam);
        }

        /// <summary>
        /// Creates an overlapped, pop-up, or child window with an extended window style.
        /// </summary>
        /// <param name="dwExStyle">The extended window style of the window being created.</param>
        /// <param name="lpClassName">A null-terminated string or a class atom created by a previous call to the
        /// RegisterClass or <see cref="RegisterClassEx"/> function.</param>
        /// <param name="lpWindowName">The window name.</param>
        /// <param name="dwStyle">The style of the window being created.</param>
        /// <param name="x">The initial horizontal position of the window.</param>
        /// <param name="y">The initial vertical position of the window.</param>
        /// <param name="nWidth">The width, in device units, of the window.</param>
        /// <param name="nHeight">The height, in device units, of the window.</param>
        /// <param name="hWndParent">A handle to the parent or owner window of the window being created.</param>
        /// <param name="hMenu">A handle to a menu, or specifies a child-window identifier, depending on the window
        /// style.</param>
        /// <param name="hInstance">A handle to the instance of the module to be associated with the window.</param>
        /// <param name="lpParam">Pointer to a value to be passed to the window through the CREATESTRUCT structure
        /// (lpCreateParams member) pointed to by the lParam param of the WM_CREATE message.</param>
        /// <returns>If the function succeeds, the return value is a handle to the new window.</returns>
        [DllImport(nameof(User32), CharSet = CharSet.Auto, SetLastError = true)]
        public static extern HWND CreateWindowEx(WindowStyleExtended dwExStyle, string lpClassName, string lpWindowName,
            WindowStyle dwStyle, int x, int y, int nWidth, int nHeight, HWND hWndParent, HMENU hMenu,
            HINSTANCE hInstance, IntPtr lpParam);

        /// <summary>
        /// Calls the default window procedure to provide default processing for any window messages that an application
        /// does not process.
        /// </summary>
        /// <param name="hWnd">A handle to the window procedure that received the message.</param>
        /// <param name="msg">The message.</param>
        /// <param name="wParam">Additional message information.</param>
        /// <param name="lParam">Additional message information.</param>
        /// <returns>The return value is the result of the message processing and depends on the message.</returns>
        [DllImport(nameof(User32), CharSet = CharSet.Auto)]
        public static extern IntPtr DefWindowProc(HWND hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        /// <summary>
        /// Dispatches a message to a window procedure.
        /// </summary>
        /// <param name="lpMsg">A pointer to a structure that contains the message.</param>
        /// <returns>The return value specifies the value returned by the window procedure.</returns>
        [DllImport(nameof(User32), CharSet = CharSet.Auto)]
        public static extern IntPtr DispatchMessage(ref MSG lpMsg);

        /// <summary>
        /// Destroys the specified window.
        /// </summary>
        /// <param name="hWnd">A handle to the window to be destroyed.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool DestroyWindow(HWND hWnd);

        /// <summary>
        /// Empties the clipboard and frees handles to data in the clipboard.
        /// </summary>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool EmptyClipboard();

        /// <summary>
        /// The EndPaint function marks the end of painting in the specified window. This function is required for each
        /// call to the <see cref="BeginPaint"/> function, but only after painting is complete.
        /// </summary>
        /// <param name="hWnd">Handle to the window that has been repainted.</param>
        /// <param name="lpPaint">Pointer to a <see cref="PAINTSTRUCT"/> structure that contains the painting
        /// information retrieved by <see cref="BeginPaint"/>.</param>
        /// <returns>The return value is always <see langword="true"/>.</returns>
        [DllImport(nameof(User32))]
        public static extern bool EndPaint(HWND hWnd, ref PAINTSTRUCT lpPaint);

        /// <summary>
        /// Retrieves the coordinates of a window's client area. The client coordinates specify the upper-left and
        /// lower-right corners of the client area. Because client coordinates are relative to the upper-left corner of
        /// a window's client area, the coordinates of the upper-left corner are (0,0).
        /// </summary>
        /// <param name="hWnd">A handle to the window whose client coordinates are to be retrieved.</param>
        /// <param name="rect">A pointer to a <see cref="RECT"/> structure that receives the client coordinates. The
        /// left and top members are zero. The right and bottom members contain the width and height of the window.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool GetClientRect(HWND hWnd, out RECT rect);

        /// <summary>
        /// Retrieves data from the clipboard in a specified format.
        /// </summary>
        /// <param name="uFormat">A <see cref="ClipboardFormat"/>.</param>
        /// <returns>If the function succeeds, the return value is the handle to a clipboard object in the specified
        /// format.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern HANDLE GetClipboardData(ClipboardFormat uFormat);

        /// <summary>
        /// Retrieves the position of the mouse cursor, in screen coordinates.
        /// </summary>
        /// <param name="lpPoint">A pointer to a POINT structure that receives the screen coordinates of the cursor.</param>
        /// <returns>Returns <see langword="true"/> if successful or <see langword="false"/> otherwise.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool GetCursorPos(out POINT lpPoint);

        /// <summary>
        /// The GetDC function retrieves a handle to a device context (DC) for the client area of a specified window or
        /// for the entire screen.
        /// </summary>
        /// <param name="hWnd">A handle to the window whose DC is to be retrieved. If this value is
        /// <see cref="IntPtr.Zero"/>, GetDC retrieves the DC for the entire screen.</param>
        /// <returns>If the function succeeds, the return value is a handle to the DC for the specified window's client
        /// area. If the function fails, the return value is <see cref="IntPtr.Zero"/>.</returns>
        [DllImport(nameof(User32))]
        public static extern HDC GetDC(HWND hWnd);

        /// <summary>
        /// Retrieves the active input locale identifier (formerly called the keyboard layout).
        /// </summary>
        /// <param name="idThread">The identifier of the thread to query, or 0 for the current thread.</param>
        /// <returns>The return value is the input locale identifier for the thread. The low word contains a Language
        /// Identifier for the input language and the high word contains a device handle to the physical layout of the
        /// keyboard.</returns>
        [DllImport(nameof(User32))]
        public static extern HKL GetKeyboardLayout(uint idThread);

        /// <summary>
        /// Retrieves the status of the specified virtual key. The status specifies whether the key is up, down, or
        /// toggled (on, off—alternating each time the key is pressed). 
        /// </summary>
        /// <param name="nVirtKey">A virtual key. If the desired virtual key is a letter or digit (A through Z, a
        /// through z, or 0 through 9), nVirtKey must be set to the ASCII value of that character. For other keys, it
        /// must be a virtual-key code.</param>
        /// <returns>The return value specifies the status of the specified virtual key.</returns>
        [DllImport(nameof(User32))]
        public static extern short GetKeyState(VirtualKey nVirtKey);

        /// <summary>
        /// Retrieves the specified system metric or system configuration setting.
        /// </summary>
        /// <param name="nIndex">The system metric or configuration setting to be retrieved.</param>
        /// <returns>If the function succeeds, the return value is the requested system metric or configuration setting.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern int GetSystemMetrics(SystemMetric nIndex);

        /// <summary>
        /// Retrieves information about the specified window. The function also retrieves the value at a specified
        /// offset into the extra window memory.
        /// </summary>
        /// <param name="hWnd">A handle to the window and, indirectly, the class to which the window belongs.</param>
        /// <param name="nIndex">The zero-based offset to the value to be retrieved.</param>
        /// <returns>If the function succeeds, the return value is the requested value.</returns>
        public static IntPtr GetWindowLong(HWND hWnd, WindowLongQuery nIndex)
            => IntPtr.Size == 4 ? GetWindowLong32(hWnd, nIndex) : GetWindowLongPtr64(hWnd, nIndex);

        /// <summary>
        /// Retrieves the dimensions of the bounding rectangle of the specified window. The dimensions are given in
        /// screen coordinates that are relative to the upper-left corner of the screen.
        /// </summary>
        /// <param name="hWnd">A handle to the window.</param>
        /// <param name="lpRect">A pointer to a <see cref="RECT"/> structure that receives the screen coordinates of the
        /// upper-left and lower-right corners of the window.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool GetWindowRect(HWND hWnd, out RECT lpRect);

        /// <summary>
        /// Loads the specified cursor resource from the executable (.EXE) file associated with an application instance.
        /// </summary>
        /// <param name="hInstance">A handle to an instance of the module whose executable file contains the cursor to
        /// be loaded.</param>
        /// <param name="lpCursorName">The name of the cursor resource to be loaded.</param>
        /// <returns>If the function succeeds, the return value is the handle to the newly loaded cursor.</returns>
        [DllImport(nameof(User32), CharSet = CharSet.Auto, SetLastError = true)]
        public static extern HCURSOR LoadCursor(HINSTANCE hInstance, int lpCursorName);

        /// <summary>
        /// Loads the specified icon resource from the executable (.exe) file associated with an application instance.
        /// </summary>
        /// <param name="hInstance">A handle to an instance of the module whose executable file contains the icon to be
        /// loaded.</param>
        /// <param name="lpIconName">The name of the icon resource to be loaded.</param>
        /// <returns></returns>
        [DllImport(nameof(User32), CharSet = CharSet.Auto, SetLastError = true)]
        public static extern HICON LoadIcon(HINSTANCE hInstance, int lpIconName);

        /// <summary>
        /// Displays a modal dialog box that contains a system icon, a set of buttons, and a brief application-specific
        /// message, such as status or error information. The message box returns an integer value that indicates which
        /// button the user clicked.
        /// </summary>
        /// <param name="hWnd">A handle to the owner window of the message box to be created.</param>
        /// <param name="lpText">The message to be displayed.</param>
        /// <param name="lpCaption">The dialog box title.</param>
        /// <param name="uType">The contents and behavior of the dialog box.</param>
        /// <returns></returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern MessageBoxResult MessageBox(IntPtr hWnd, string lpText, string lpCaption,
            MessageBoxType uType = MessageBoxType.MB_OK);

        /// <summary>
        /// Opens the clipboard for examination and prevents other applications from modifying the clipboard content.
        /// </summary>
        /// <param name="hWndNewOwner">A handle to the window to be associated with the open clipboard.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool OpenClipboard(HWND hWndNewOwner);

        /// <summary>
        /// Dispatches incoming sent messages, checks the thread message queue for a posted message, and retrieves the
        /// message (if any exist).
        /// </summary>
        /// <param name="lpMsg">A pointer to an MSG structure that receives message information.</param>
        /// <param name="hWnd">A handle to the window whose messages are to be retrieved.</param>
        /// <param name="wMsgFilterMin">The value of the first message in the range of messages to be examined.</param>
        /// <param name="wMsgFilterMax">The value of the last message in the range of messages to be examined.</param>
        /// <param name="wRemoveMsg">Specifies how messages are to be handled.</param>
        /// <returns>If a message is available, the return value is <see langword="true"/>.</returns>
        /// <remarks><paramref name="lpMsg"/> must not be marked as <see langword="out"/> to prevent crashes.</remarks>
        [DllImport(nameof(User32), CharSet = CharSet.Auto)]
        public static extern bool PeekMessage(ref MSG lpMsg, HWND hWnd, uint wMsgFilterMin, uint wMsgFilterMax,
            PeekMessageRemove wRemoveMsg);

        /// <summary>
        /// Places (posts) a message in the message queue associated with the thread that created the specified window
        /// and returns without waiting for the thread to process the message.
        /// </summary>
        /// <param name="hWnd">A handle to the window whose window procedure is to receive the message.</param>
        /// <param name="msg">The message to be posted.</param>
        /// <param name="wParam">Additional message-specific information.</param>
        /// <param name="lParam">Additional message-specific information.</param>
        /// <remarks>If the function succeeds, the return value is <see langword="true"/>.</remarks>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool PostMessage(HWND hWnd, uint msg, IntPtr wParam, IntPtr lParam);

        /// <summary>
        /// Indicates to the system that a thread has made a request to terminate (quit).
        /// </summary>
        /// <param name="nExitCode">The application exit code.</param>
        [DllImport(nameof(User32))]
        public static extern void PostQuitMessage(int nExitCode);

        /// <summary>
        /// Registeres a window class for subsequent use in calls to the <see cref="CreateWindow"/> or
        /// <see cref="CreateWindowEx"/> function.
        /// </summary>
        /// <param name="lpwWndClass">A pointer to a <see cref="WNDCLASS"/> structure.</param>
        /// <returns>If the function succeeds, the return value is a class atom that uniquely identifies the class being
        /// registered.</returns>
        [DllImport(nameof(User32), CharSet = CharSet.Auto, SetLastError = true)]
        public static extern ushort RegisterClass(ref WNDCLASS lpwWndClass);

        /// <summary>
        /// Registers a window class for subsequent use in calls to the CreateWindow or <see cref="CreateWindowEx"/>
        /// function.
        /// </summary>
        /// <param name="lpwcx">A pointer to a <see cref="WNDCLASSEX"/> structure.</param>
        /// <returns>If the function succeeds, the return value is a class atom that uniquely identifies the class being
        /// registered.</returns>
        [DllImport(nameof(User32), CharSet = CharSet.Auto, SetLastError = true)]
        public static extern ushort RegisterClassEx(ref WNDCLASSEX lpwcx);

        /// <summary>
        /// Releases the mouse capture from a window in the current thread and restores normal mouse input processing.
        /// </summary>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool ReleaseCapture();

        /// <summary>
        /// Releases a device context (DC), freeing it for use by other applications. The effect of the ReleaseDC
        /// function depends on the type of DC. It frees only common and window DCs. It has no effect on class or
        /// private DCs.
        /// </summary>
        /// <param name="hWnd">A handle to the window whose DC is to be released.</param>
        /// <param name="hDC">A handle to the DC to be released.</param>
        /// <returns>The return value indicates whether the DC was released. If the DC was released, the return value is
        /// <c>1</c>. If the DC was not released, the return value is <c>0</c>.</returns>
        [DllImport(nameof(User32))]
        public static extern int ReleaseDC(HWND hWnd, HDC hDC);

        /// <summary>
        /// Converts the screen coordinates of a specified point on the screen to client-area coordinates.
        /// </summary>
        /// <param name="hWnd">A handle to the window whose client area will be used for the conversion.</param>
        /// <param name="lpPoint">A pointer to a POINT structure that specifies the screen coordinates to be converted.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32))]
        public static extern bool ScreenToClient(HWND hWnd, ref POINT lpPoint);

        /// <summary>
        /// Sends the specified message to a window or windows.
        /// </summary>
        /// <param name="hWnd">A handle to the window whose window procedure will receive the message.</param>
        /// <param name="msg">The message to be sent.</param>
        /// <param name="wParam">Additional message-specific information.</param>
        /// <param name="lParam">Additional message-specific information.</param>
        /// <returns>The return value specifies the result of the message processing; it depends on the message sent.</returns>
        [DllImport(nameof(User32))]
        public static extern IntPtr SendMessage(HWND hWnd, WindowMessage msg, IntPtr wParam, IntPtr lParam);

        /// <summary>
        /// Sends the specified message to a window or windows.
        /// </summary>
        /// <param name="hWnd">A handle to the window whose window procedure will receive the message.</param>
        /// <param name="msg">The message to be sent.</param>
        /// <param name="wParam">Additional message-specific information.</param>
        /// <param name="lParam">Additional message-specific information.</param>
        /// <returns>The return value specifies the result of the message processing; it depends on the message sent.</returns>
        [DllImport(nameof(User32))]
        public static extern IntPtr SendMessage(HWND hWnd, WindowMessage msg, ref KBDLLHOOKSTRUCT wParam, IntPtr lParam);

        /// <summary>
        /// Places data on the clipboard in a specified <see cref="ClipboardFormat"/>.
        /// </summary>
        /// <param name="uFormat">The <see cref="ClipboardFormat"/>.</param>
        /// <param name="hMem">A handle to the data in the specified format. </param>
        /// <returns>If the function succeeds, the return value is the handle to the data.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern HANDLE SetClipboardData(ClipboardFormat uFormat, HANDLE hMem);

        /// <summary>
        /// Sets the cursor shape.
        /// </summary>
        /// <param name="hCursor">A handle to the cursor.</param>
        /// <returns>The return value is the handle to the previous cursor, if there was one.</returns>
        [DllImport(nameof(User32), CharSet = CharSet.Auto)]
        public static extern IntPtr SetCursor(IntPtr hCursor);

        /// <summary>
        /// Sets the keyboard focus to the specified window.
        /// </summary>
        /// <param name="hWnd">A handle to the window that will receive the keyboard input.</param>
        /// <returns>If the function succeeds, the return value is the handle to the window that previously had the
        /// keyboard focus.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern HWND SetFocus(HWND hWnd);

        /// <summary>
        /// Changes an attribute of the specified window.
        /// </summary>
        /// <param name="hWnd">A handle to the window and, indirectly, the class to which the window belongs.</param>
        /// <param name="nIndex">The zero-based offset to the value to be set.</param>
        /// <param name="dwNewLong">The replacement value.</param>
        /// <returns>If the function succeeds, the return value is the previous value of the specified offset.</returns>
        public static IntPtr SetWindowLong(HWND hWnd, WindowLongQuery nIndex, IntPtr dwNewLong)
            => IntPtr.Size == 4 ? SetWindowLong32(hWnd, nIndex, dwNewLong) : SetWindowLongPtr64(hWnd, nIndex, dwNewLong);

        /// <summary>
        /// Changes the size, position, and Z order of a child, pop-up, or top-level window.
        /// </summary>
        /// <param name="hWnd">A handle to the window.</param>
        /// <param name="hWndInsertAfter">A handle to the window to precede the positioned window in the Z order.</param>
        /// <param name="x">The new position of the left side of the window, in client coordinates.</param>
        /// <param name="y">The new position of the top of the window, in client coordinates.</param>
        /// <param name="cx">The new width of the window, in pixels.</param>
        /// <param name="cy">The new height of the window, in pixels.</param>
        /// <param name="uFlags">The window sizing and positioning flags.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool SetWindowPos(HWND hWnd, HWND hWndInsertAfter, int x, int y, int cx, int cy,
            SetWindowPosFlags uFlags);

        /// <summary>
        /// Installs an application-defined hook procedure into a hook chain.
        /// </summary>
        /// <param name="idHook">The type of hook procedure to be installed.</param>
        /// <param name="lpfn">A pointer to the hook procedure.</param>
        /// <param name="hmod">A handle to the DLL containing the hook procedure pointed to by the lpfn parameter.</param>
        /// <param name="dwThreadId">The identifier of the thread with which the hook procedure is to be associated.</param>
        /// <returns>If the function succeeds, the return value is the handle to the hook procedure.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern HHOOK SetWindowsHookEx(WindowsHook idHook, LowLevelKeyboardProc lpfn, HINSTANCE hmod,
            uint dwThreadId);

        /// <summary>
        /// Displays or hides the cursor.
        /// </summary>
        /// <param name="bShow">If bShow is <see langword="true"/>, the display count is incremented by one. If bShow is
        /// <see langword="false"/>, the display count is decremented by one.</param>
        /// <returns>The return value specifies the new display counter.</returns>
        [DllImport(nameof(User32))]
        public static extern int ShowCursor(bool bShow);

        /// <summary>
        /// Sets the specified window's show state.
        /// </summary>
        /// <param name="hWnd">A handle to the window.</param>
        /// <param name="nCmdShow">Controls how the window is to be shown.</param>
        /// <returns>If the window was previously visible, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32))]
        public static extern bool ShowWindow(HWND hWnd, ShowWindowCmd nCmdShow);

        /// <summary>
        /// Retrieves or sets the value of one of the system-wide parameters.
        /// </summary>
        /// <param name="uiAction">The system-wide parameter to be retrieved or set.</param>
        /// <param name="uiParam">A parameter whose usage and format depends on the system parameter being queried or
        /// set.</param>
        /// <param name="pvParam">A parameter whose usage and format depends on the system parameter being queried or
        /// set.</param>
        /// <param name="fWinIni">If a system parameter is being set, specifies whether the user profile is to be
        /// updated, and if so, whether the WM_SETTINGCHANGE message is to be broadcast to all top-level windows to
        /// notify them of the change.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool SystemParametersInfo(SysParameter uiAction, uint uiParam, ref RECT pvParam,
            SysParameterChange fWinIni);

        /// <summary>
        /// Retrieves or sets the value of one of the system-wide parameters.
        /// </summary>
        /// <param name="uiAction">The system-wide parameter to be retrieved or set.</param>
        /// <param name="uiParam">A parameter whose usage and format depends on the system parameter being queried or
        /// set.</param>
        /// <param name="pvParam">A parameter whose usage and format depends on the system parameter being queried or
        /// set.</param>
        /// <param name="fWinIni">If a system parameter is being set, specifies whether the user profile is to be
        /// updated, and if so, whether the WM_SETTINGCHANGE message is to be broadcast to all top-level windows to
        /// notify them of the change.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool SystemParametersInfo(SysParameter uiAction, uint uiParam, ref STICKYKEYS pvParam,
            SysParameterChange fWinIni);

        /// <summary>
        /// Translates virtual-key messages into character messages. The character messages are posted to the calling
        /// thread's message queue, to be read the next time the thread calls the GetMessage or
        /// <see cref="PeekMessage(ref MSG, HWND, UInt32, UInt32, PeekMessageRemove)"/> function.
        /// </summary>
        /// <param name="lpMsg">A pointer to a <see cref="MSG"/> structure that contains message information retrieved
        /// from the calling thread's message queue.</param>
        /// <returns>If the message is translated (that is, a character message is posted to the thread's message
        /// queue), the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32))]
        public static extern bool TranslateMessage(ref MSG lpMsg);

        /// <summary>
        /// Removes a hook procedure installed in a hook chain by the SetWindowsHookEx function.
        /// </summary>
        /// <param name="hhk">A handle to the hook to be removed.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32), SetLastError = true)]
        public static extern bool UnhookWindowsHookEx(HHOOK hhk);

        /// <summary>
        /// The UpdateWindow function updates the client area of the specified window by sending a WM_PAINT message to
        /// the window if the window's update region is not empty.
        /// </summary>
        /// <param name="hWnd">Handle to the window to be updated.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(User32))]
        public static extern bool UpdateWindow(HWND hWnd);

        // ---- DELEGATES ----------------------------------------------------------------------------------------------

        /// <summary>
        /// An application-defined or library-defined callback function used with the SetWindowsHookEx function. The
        /// system calls this function every time a new keyboard input event is about to be posted into a thread input queue.
        /// </summary>
        /// <param name="nCode">A code the hook procedure uses to determine how to process the message.</param>
        /// <param name="wParam">The identifier of the keyboard message.</param>
        /// <param name="lParam">A pointer to a KBDLLHOOKSTRUCT structure.</param>
        /// <returns>If nCode is less than zero, the hook procedure must return the value returned by CallNextHookEx.
        /// </returns>
        public delegate IntPtr LowLevelKeyboardProc(HookCode nCode, ref WindowMessage wParam, ref KBDLLHOOKSTRUCT lParam);

        /// <summary>
        /// An application-defined function that processes messages sent to a window.
        /// </summary>
        /// <param name="hwnd">A handle to the window.</param>
        /// <param name="uMsg">The message.</param>
        /// <param name="wParam">Additional message information.</param>
        /// <param name="lParam">Additional message information.</param>
        /// <returns>The return value is the result of the message processing and depends on the message sent.</returns>
        public delegate IntPtr WNDPROC(HWND hwnd, uint uMsg, IntPtr wParam, IntPtr lParam);

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        [DllImport(nameof(User32), EntryPoint = "GetWindowLong", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetWindowLong32(HWND hWnd, WindowLongQuery nIndex);

        [DllImport(nameof(User32), EntryPoint = "GetWindowLongPtr", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetWindowLongPtr64(HWND hWnd, WindowLongQuery nIndex);

        [DllImport(nameof(User32), EntryPoint = "SetWindowLong", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowLong32(HWND hWnd, WindowLongQuery nIndex, IntPtr dwNewLong);

        [DllImport(nameof(User32), EntryPoint = "SetWindowLongPtr", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr SetWindowLongPtr64(HWND hWnd, WindowLongQuery nIndex, IntPtr dwNewLong);
    }
}
