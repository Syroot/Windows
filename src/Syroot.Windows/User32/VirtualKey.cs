﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents a key on the keyboard.
        /// </summary>
        public enum VirtualKey : int
        {
            VK_RETURN = 0xD,
            VK_SHIFT = 0x10,
            VK_CONTROL = 0x11,
            /// <summary>IME final mode</summary>
            VK_FINAL = 0x18,
            VK_ESCAPE = 0x1B,
            VK_SPACE = 0x20,
            VK_END = 0x23,
            VK_HOME = 0x24,
            VK_LEFT = 0x25,
            VK_UP = 0x26,
            VK_RIGHT = 0x27,
            VK_DOWN = 0x28,
            VK_SNAPSHOT = 0x2C,
            VK_INSERT = 0x2D,
            VK_DELETE = 0x2E,
            VK_LWIN = 0x5B,
            VK_RWIN = 0x5C,
            VK_F4 = 0x73,
            VK_F5 = 0x74,
            VK_F6 = 0x75,
            VK_F7 = 0x76,
            VK_F8 = 0x77,
            VK_F9 = 0x78,
            VK_F10 = 0x79,
            VK_F11 = 0x7A,
            VK_F12 = 0x7B,
            VK_F13 = 0x7C,
            VK_F14 = 0x7D,
            VK_F15 = 0x7E,
            VK_F16 = 0x7F,
            VK_F17 = 0x80,
            VK_F18 = 0x81,
            VK_F19 = 0x82,
            VK_F20 = 0x83,
            VK_F21 = 0x84,
            VK_F22 = 0x85,
            VK_F23 = 0x86,
            VK_F24 = 0x87,
            VK_PROCESSKEY = 0xE5
        }
    }
}
