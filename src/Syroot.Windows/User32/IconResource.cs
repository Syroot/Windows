﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents named icon resource IDs.
        /// </summary>
        public enum IconResource : int
        {
            /// <summary>Default application icon.</summary>
            IDI_APPLICATION = 32512,
            /// <summary>Hand-shaped icon. Same as <see cref="IDI_ERROR"/>.</summary>
            IDI_HAND = 32513,
            /// <summary>Question mark icon.</summary>
            IDI_QUESTION = 32514,
            /// <summary>Exclamation point icon. Same as <see cref="IDI_WARNING"/>.</summary>
            IDI_EXCLAMATION = 32515,
            /// <summary>Asterisk icon. Same as <see cref="IDI_INFORMATION"/>.</summary>
            IDI_ASTERISK = 32516,
            /// <summary>Default application icon.</summary>
            IDI_WINLOGO = 32517,
            /// <summary>Security shield icon.</summary>
            IDI_SHIELD = 32518,
            /// <summary>Exclamation point icon.</summary>
            IDI_WARNING = IDI_EXCLAMATION,
            /// <summary>Hand-shaped icon.</summary>
            IDI_ERROR = IDI_HAND,
            /// <summary>Asterisk icon.</summary>
            IDI_INFORMATION = IDI_ASTERISK
        }
    }
}
