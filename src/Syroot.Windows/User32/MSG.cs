﻿using System;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Contains message information from a thread's message queue.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct MSG
        {
            // ---- FIELDS ---------------------------------------------------------------------------------------------

            /// <summary>A handle to the window whose window procedure receives the message.</summary>
            public HWND hwnd;
            /// <summary>The message identifier.</summary>
            public uint message;
            /// <summary>Additional information about the message.</summary>
            public IntPtr wParam;
            /// <summary>Additional information about the message.</summary>
            public IntPtr lParam;
            /// <summary>The time at which the message was posted.</summary>
            public uint time;
            /// <summary>The cursor position, in screen coordinates, when the message was posted.</summary>
            public POINT pt;
        }
    }
}
