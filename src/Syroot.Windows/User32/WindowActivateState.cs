﻿namespace Syroot.Windows
{
    public static partial class User32
    {
        /// <summary>
        /// Represents <see cref="WindowMessage.WM_ACTIVATE"/> wParam state values.
        /// </summary>
        public enum WindowActivateState
        {
            Inactive,
            Active,
            ClickActive
        }
    }
}
