﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    [DebuggerDisplay("{_pointer,h}")]
    [StructLayout(LayoutKind.Sequential)]
    public struct HICON
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly IntPtr _pointer;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public HICON(IntPtr pointer) => _pointer = pointer;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static implicit operator IntPtr(HICON handle) => handle._pointer;

        public static implicit operator HICON(IntPtr pointer) => new HICON(pointer);
    }
}
