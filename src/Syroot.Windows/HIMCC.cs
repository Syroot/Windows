﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    [DebuggerDisplay("{_pointer,h}")]
    [StructLayout(LayoutKind.Sequential)]
    public struct HIMCC
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly IntPtr _pointer;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public HIMCC(IntPtr pointer) => _pointer = pointer;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static implicit operator IntPtr(HIMCC handle) => handle._pointer;

        public static implicit operator HIMCC(IntPtr pointer) => new HIMCC(pointer);
    }
}
