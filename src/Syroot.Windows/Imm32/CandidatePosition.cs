﻿namespace Syroot.Windows
{
    public static partial class Imm32
    {
        /// <summary>
        /// Special indices of a candidate list.
        /// </summary>
        public enum CandidatePosition : uint
        {
            CPS_COMPLETE = 1,
            CPS_CONVERT = 2,
            CPS_REVERT = 3,
            CPS_CANCEL = 4
        }
    }
}
