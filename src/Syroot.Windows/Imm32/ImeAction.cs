﻿namespace Syroot.Windows
{
    public static partial class Imm32
    {
        /// <summary>
        /// IME notification code.
        /// </summary>
        public enum ImeAction : uint
        {
            /// <summary>An application directs the IME to open a candidate list.</summary>
            NI_OPENCANDIDATE = 0x0010,
            /// <summary>An application directs the IME to close a candidate list.</summary>
            NI_CLOSECANDIDATE = 0x0011,
            /// <summary>An application has selected one of the candidates.</summary>
            NI_SELECTCANDIDATESTR = 0x0012,
            /// <summary>An application changed the current selected candidate.</summary>
            NI_CHANGECANDIDATELIST = 0x0013,
            NI_FINALIZECONVERSIONRESULT = 0x0014,
            /// <summary>An application directs the IME to carry out an action on the composition string.</summary>
            NI_COMPOSITIONSTR = 0x0015,
            /// <summary>The application changes the page starting index of a candidate list.</summary>
            NI_SETCANDIDATE_PAGESTART = 0x0016,
            /// <summary>The application changes the page size of a candidate list.</summary>
            NI_SETCANDIDATE_PAGESIZE = 0x0017,
            /// <summary>An application directs the IME to allow the application to handle the specified menu.</summary>
            NI_IMEMENUSELECTED = 0x0018
        }
    }
}
