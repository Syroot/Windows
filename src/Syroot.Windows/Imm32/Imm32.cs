﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Syroot.Windows
{
    /// <summary>
    /// Represents definitions in the Windows Input Method Manager library as part of the WinAPI.
    /// </summary>
    public static partial class Imm32
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Associates the specified input context with the specified window. By default, the operating system
        /// associates the default input context with each window as it is created.
        /// </summary>
        /// <param name="hwnd">Handle to the window to associate with the input context.</param>
        /// <param name="himc">Handle to the input context. If hIMC is NULL, the function removes any association the
        /// window has with an input context. Thus IME cannot be used in the window.</param>
        /// <returns>Returns the handle to the input context previously associated with the window.</returns>
        [DllImport(nameof(Imm32))]
        public static extern HIMC ImmAssociateContext(HWND hwnd, HIMC himc);

        /// <summary>
        /// Disables the text service for the specified thread.
        /// </summary>
        /// <param name="idThread">Identifier of the thread for which to disable the text service.</param>
        /// <returns>Returns <see langword="true"/> if successful or <see langword="false"/> otherwise.</returns>
        [DllImport(nameof(Imm32))]
        public static extern bool ImmDisableTextFrameService(uint idThread);

        /// <summary>
        /// Retrieves information about the composition string.
        /// </summary>
        /// <param name="himc">Handle to the input context.</param>
        /// <param name="index">Index of the information to retrieve, which is one of the values specified in
        /// IME Composition String Values.</param>
        /// <param name="lpBuf">Pointer to a buffer in which the function retrieves the composition string information.</param>
        /// <param name="dwBufLen">Size, in bytes, of the output buffer, even if the output is a Unicode string.</param>
        /// <returns>Returns the number of bytes copied to the output buffer.</returns>
        [DllImport(nameof(Imm32), CharSet = CharSet.Auto)]
        public static extern int ImmGetCompositionString(HIMC himc, ImeCompositionString index, IntPtr lpBuf,
            uint dwBufLen);

        /// <summary>
        /// Retrieves a candidate list.
        /// </summary>
        /// <param name="himc">Handle to the input context.</param>
        /// <param name="deIndex">Zero-based index of the candidate list.</param>
        /// <param name="lpCandList">Pointer to a CANDIDATELIST structure in which the function retrieves the candidate
        /// list.</param>
        /// <param name="dwBufLen">Size, in bytes, of the buffer to receive the candidate list. The application can
        /// specify 0 for this parameter if the function is to return the required size of the output buffer only.</param>
        /// <returns>Returns the number of bytes copied to the candidate list buffer if successful. If the application
        /// has supplied 0 for the dwBufLen parameter, the function returns the size required for the candidate list
        /// buffer.</returns>
        [DllImport(nameof(Imm32), CharSet = CharSet.Auto)]
        public static extern uint ImmGetCandidateList(HIMC himc, uint deIndex, IntPtr lpCandList, uint dwBufLen);

        /// <summary>
        /// Returns the input context associated with the specified window.
        /// </summary>
        /// <param name="hwnd">Handle to the window for which to retrieve the input context.</param>
        /// <returns>Returns the handle to the input context.</returns>
        [DllImport(nameof(Imm32))]
        public static extern HIMC ImmGetContext(HWND hwnd);

        /// <summary>
        /// Retrieves the current conversion status.
        /// </summary>
        /// <param name="himc">Handle to the input context for which to retrieve status information.</param>
        /// <param name="lpfdwConversion">Pointer to a variable in which the function retrieves a combination of
        /// conversion mode values.</param>
        /// <param name="lpfdwSentence">Pointer to a variable in which the function retrieves a sentence mode value.</param>
        /// <returns>Returns <see langword="true"/> if successful, or <see langword="false"/> otherwise.</returns>
        [DllImport(nameof(Imm32))]
        public static extern bool ImmGetConversionStatus(HIMC himc, out uint lpfdwConversion, out uint lpfdwSentence);

        /// <summary>
        /// Retrieves the default window handle to the IME class.
        /// </summary>
        /// <param name="hwnd">Handle to the window.</param>
        /// <returns>Returns the default window handle to the IME class if successful, or NULL otherwise.</returns>
        [DllImport(nameof(Imm32))]
        public static extern HWND ImmGetDefaultIMEWnd(HWND hwnd);

        /// <summary>
        /// Retrieves the file name of the IME associated with the specified input locale.
        /// </summary>
        /// <param name="hkl">Input locale identifier.</param>
        /// <param name="lpszFileName">Pointer to a buffer in which the function retrieves the file name. This parameter
        /// contains 0 when uBufLen is set to 0.</param>
        /// <param name="uBufLen">Size, in bytes, of the output buffer. The application specifies 0 if the function is
        /// to return the buffer size needed to receive the file name, not including the terminating null character. For
        /// Unicode, uBufLen specifies the size in Unicode characters, not including the terminating null character.</param>
        /// <returns>Returns the number of bytes in the file name copied to the output buffer.</returns>
        [DllImport(nameof(Imm32), CharSet = CharSet.Auto)]
        public static extern uint ImmGetIMEFileName(HKL hkl, StringBuilder lpszFileName, uint uBufLen);

        /// <summary>
        /// Determines whether the IME is open or closed.
        /// </summary>
        /// <param name="himc">Handle to the input context.</param>
        /// <returns>Returns <see langword="true"/> if the IME is open, or <see langword="false"/> otherwise.</returns>
        [DllImport(nameof(Imm32))]
        public static extern bool ImmGetOpenStatus(HIMC himc);

        /// <summary>
        /// Retrieves the original virtual key value associated with a key input message that the IME has already
        /// processed.
        /// </summary>
        /// <param name="hwnd">Handle to the window that receives the key message.</param>
        /// <returns>If TranslateMessage has been called by the application, returns
        /// <see cref="User32.VirtualKey.VK_PROCESSKEY"/>; otherwise, it returns the virtual key.</returns>
        [DllImport(nameof(Imm32))]
        public static extern uint ImmGetVirtualKey(HWND hwnd);

        /// <summary>
        /// Determines if the specified input locale has an IME.
        /// </summary>
        /// <param name="hkl">Input locale identifier.</param>
        /// <returns>Returns <see langword="true"/> if the specified locale has an IME, or <see langword="false"/>
        /// otherwise.</returns>
        [DllImport(nameof(Imm32))]
        public static extern bool ImmIsIME(HKL hkl);

        /// <summary>
        /// This function enables an input method editor (IME) to access the INPUTCONTEXT structure for an input method
        /// context (IMC) by returning a pointer to the structure.
        /// </summary>
        /// <param name="himc">Handle to the IMC.</param>
        /// <returns>Pointer to INPUTCONTEXT indicates success. NULL indicates failure.</returns>
        [DllImport(nameof(Imm32))]
        public static extern IntPtr ImmLockIMC(HIMC himc);

        /// <summary>
        /// This function enables an input method editor (IME) to get a pointer to an input method context (IMC)
        /// component that can be a member of the IMC.
        /// </summary>
        /// <param name="himcc">Handle to IMC component.</param>
        /// <returns>Pointer to the IMC component indicates success. NULL indicates failure.</returns>
        [DllImport(nameof(Imm32))]
        public static extern IntPtr ImmLockIMCC(HIMCC himcc);

        /// <summary>
        /// Notifies the IME about changes to the status of the input context.
        /// </summary>
        /// <param name="himc">Handle to the input context.</param>
        /// <param name="dwAction">Notification code.</param>
        /// <param name="dwIndex">Index of a candidate list. Alternatively, if dwAction is NI_COMPOSITIONSTR, this
        /// parameter can have one of the following values.</param>
        /// <param name="dwValue">Index of a candidate string. The application can set this parameter or ignore it,
        /// depending on the value of the dwAction parameter.</param>
        /// <returns>Returns <see langword="true"/> if successful, or <see langword="false"/> otherwise.</returns>
        [DllImport(nameof(Imm32))]
        public static extern bool ImmNotifyIME(HIMC himc, ImeAction dwAction, uint dwIndex, uint dwValue);

        /// <summary>
        /// Releases the input context and unlocks the memory associated in the input context.
        /// </summary>
        /// <param name="hwnd">Handle to the window for which the input context was previously retrieved.</param>
        /// <param name="himc">Handle to the input context.</param>
        /// <returns>Returns <see langword="true"/> if successful, or <see langword="false"/> otherwise.</returns>
        [DllImport(nameof(Imm32))]
        public static extern bool ImmReleaseContext(HWND hwnd, HIMC himc);

        /// <summary>
        /// Opens or closes the IME.
        /// </summary>
        /// <param name="himc">Handle to the input context.</param>
        /// <param name="open">TRUE if the IME is open, or FALSE if it is closed.</param>
        /// <returns>Returns <see langword="true"/> if successful, or <see langword="false"/> otherwise.</returns>
        [DllImport(nameof(Imm32))]
        public static extern bool ImmSetOpenStatus(HIMC himc, bool open);

        /// <summary>
        /// Sets the current conversion status.
        /// </summary>
        /// <param name="himc">Handle to the input context.</param>
        /// <param name="p1">Conversion mode values.</param>
        /// <param name="p2">Conversion mode values.</param>
        /// <returns>Returns <see langword="true"/> if successful, or <see langword="false"/> otherwise.</returns>
        [DllImport(nameof(Imm32))]
        public static extern bool ImmSetConversionStatus(HIMC himc, uint p1, uint p2);

        /// <summary>
        /// Simulates the specified IME hot key, causing the same response as if the user presses the hot key in the
        /// specified window.
        /// </summary>
        /// <param name="hwnd">Handle to the window.</param>
        /// <param name="hotKeyIdentifier">Identifier of the IME hot key.</param>
        /// <returns>Returns <see langword="true"/> if successful, or <see langword="false"/> otherwise.</returns>
        [DllImport(nameof(Imm32))]
        public static extern bool ImmSimulateHotKey(HWND hwnd, uint hotKeyIdentifier);

        /// <summary>
        /// This function reduces the lock count for the input method context (IMC).
        /// </summary>
        /// <param name="himc">Handle to the IMC.</param>
        /// <returns>FALSE when the lock count of IMC is reduced to zero; otherwise, TRUE.</returns>
        [DllImport(nameof(Imm32))]
        public static extern bool ImmUnlockIMC(HIMC himc);

        /// <summary>
        /// This function reduces the lock count for the input method context (IMC) component.
        /// </summary>
        /// <param name="himcc">Handle to the IMC component.</param>
        /// <returns>FALSE indicates that the lock count of IMC component is reduced to zero; otherwise, TRUE.</returns>
        [DllImport(nameof(Imm32))]
        public static extern bool ImmUnlockIMCC(HIMCC himcc);
    }
}
