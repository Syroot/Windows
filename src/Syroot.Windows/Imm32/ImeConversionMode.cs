﻿namespace Syroot.Windows
{
    public static partial class Imm32
    {
        /// <summary>
        /// Represents bit fields for conversion mode. These values are used with the ImmGetConversionStatus and
        /// ImmSetConversionStatus functions.
        /// </summary>
        public enum ImeConversionMode
        {
            IME_CMODE_NATIVE = 0x0001,
            IME_CMODE_CHINESE = IME_CMODE_NATIVE,
            IME_CMODE_HANGUL = IME_CMODE_NATIVE,
            IME_CMODE_JAPANESE = IME_CMODE_NATIVE
        }
    }
}
