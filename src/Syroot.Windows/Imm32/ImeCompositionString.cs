﻿using System;

namespace Syroot.Windows
{
    public static partial class Imm32
    {
        /// <summary>
        /// These values are used with ImmGetCompositionString and WM_IME_COMPOSITION.
        /// </summary>
        [Flags]
        public enum ImeCompositionString : uint
        {
            /// <summary>Retrieve or update the reading string of the current composition.</summary>
            GCS_COMPREADSTR = 1 << 0,
            /// <summary>Retrieve or update the attributes of the reading string of the current composition.</summary>
            GCS_COMPREADATTR = 1 << 1,
            /// <summary>Retrieve or update the clause information of the reading string of the composition string.</summary>
            GCS_COMPREADCLAUSE = 1 << 2,
            /// <summary>Retrieve or update the current composition string.</summary>
            GCS_COMPSTR = 1 << 3,
            /// <summary>Retrieve or update the attribute of the composition string.</summary>
            GCS_COMPATTR = 1 << 4,
            /// <summary>Retrieve or update clause information of the composition string.</summary>
            GCS_COMPCLAUSE = 1 << 5,
            /// <summary>Retrieve or update the cursor position in composition string.</summary>
            GCS_CURSORPOS = 1 << 7,
            /// <summary>Retrieve or update the starting position of any changes in composition string.</summary>
            GCS_DELTASTART = 1 << 8,
            /// <summary>Retrieve or update the reading string.</summary>
            GCS_RESULTREADSTR = 1 << 9,
            /// <summary>Retrieve or update clause information of the reading string.</summary>
            GCS_RESULTREADCLAUSE = 1 << 10,
            /// <summary>Retrieve or update the string of the composition result.</summary>
            GCS_RESULTSTR = 1 << 11,
            /// <summary>Retrieve or update clause information of the result string.</summary>
            GCS_RESULTCLAUSE = 1 << 12
        }
    }
}
