﻿namespace Syroot.Windows
{
    public static partial class Imm32
    {
        public enum ImeNotification
        {
            /// <summary>Notifies an application when an IME is about to close the status window.</summary>
            IMN_CLOSESTATUSWINDOW = 1,
            /// <summary>Notifies an application when an IME is about to create the status window.</summary>
            IMN_OPENSTATUSWINDOW,
            /// <summary>Notifies the application when an IME is about to change the content of the candidate window.</summary>
            IMN_CHANGECANDIDATE,
            /// <summary>Notifies an application when an IME is about to close the candidates window.</summary>
            IMN_CLOSECANDIDATE,
            /// <summary>Notifies an application when an IME is about to open the candidate window.</summary>
            IMN_OPENCANDIDATE,
            /// <summary>Notifies an application when the conversion mode of the input context is updated.</summary>
            IMN_SETCONVERSIONMODE,
            /// <summary>Notifies an application when the sentence mode of the input context is updated.</summary>
            IMN_SETSENTENCEMODE,
            /// <summary>Notifies an application when the open status of the input context is updated.</summary>
            IMN_SETOPENSTATUS,
            /// <summary>Notifies an application when candidate processing has finished and the IME is about to move the
            /// candidate window.</summary>
            IMN_SETCANDIDATEPOS,
            /// <summary>Notifies an application when the font of the input context is updated.</summary>
            IMN_SETCOMPOSITIONFONT,
            /// <summary>Notifies an application when the style or position of the composition window is updated.</summary>
            IMN_SETCOMPOSITIONWINDOW,
            /// <summary>Notifies an application when the status window position in the input context is updated.</summary>
            IMN_SETSTATUSWINDOWPOS,
            /// <summary>Notifies an application when an IME is about to show an error message or other information.</summary>
            IMN_GUIDELINE,
            IMN_PRIVATE
        }
    }
}
