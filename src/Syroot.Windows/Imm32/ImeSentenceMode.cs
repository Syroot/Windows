﻿namespace Syroot.Windows
{
    public static partial class Imm32
    {
        /// <summary>
        /// Represents bit fields for sentence mode. These values are used with the ImmGetConversionStatus and
        /// ImmSetConversionStatus functions.
        /// </summary>
        public enum ImeSentenceMode
        {
            IME_SMODE_NONE = 0,
            IME_SMODE_PLAURALCLAUSE = 1 << 0,
            IME_SMODE_SINGLECONVERT = 1 << 1,
            IME_SMODE_AUTOMATIC = 1 << 2,
            IME_SMODE_PHRASEPREDICT = 1 << 3,
            IME_SMODE_CONVERSATION = 1 << 4
        }
    }
}
