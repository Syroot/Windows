﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    [DebuggerDisplay("{_pointer,h}")]
    [StructLayout(LayoutKind.Sequential)]
    public struct HHOOK
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        public IntPtr _pointer;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public HHOOK(IntPtr pointer) => _pointer = pointer;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static implicit operator IntPtr(HHOOK handle) => handle._pointer;

        public static implicit operator HHOOK(IntPtr pointer) => new HHOOK(pointer);
    }
}
