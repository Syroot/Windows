﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    [DebuggerDisplay("{_pointer,h}")]
    [StructLayout(LayoutKind.Sequential)]
    public struct HMODULE
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly IntPtr _pointer;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public HMODULE(IntPtr pointer) => _pointer = pointer;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static implicit operator IntPtr(HMODULE handle) => handle._pointer;

        public static implicit operator HMODULE(IntPtr pointer) => new HMODULE(pointer);
    }
}
