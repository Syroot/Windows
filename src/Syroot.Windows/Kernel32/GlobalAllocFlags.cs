﻿using System;

namespace Syroot.Windows
{
    public static partial class Kernel32
    {
        [Flags]
        public enum GlobalAllocFlags : uint
        {
            /// <summary>Allocates fixed memory. The return value is a pointer.</summary>
            GMEM_FIXED,
            /// <summary>Allocates movable memory.</summary>
            GMEM_MOVEABLE = 1 << 1,
            /// <summary>Initializes memory contents to zero.</summary>
            GMEM_ZEROINIT = 1 << 6,
            /// <summary>Combines <see cref="GMEM_FIXED"/> and <see cref="GMEM_ZEROINIT"/>.</summary>
            GPTR = GMEM_FIXED | GMEM_ZEROINIT,
            /// <summary>Combines <see cref="GMEM_MOVEABLE"/> and <see cref="GMEM_ZEROINIT"/>.</summary>
            GHND = GMEM_MOVEABLE | GMEM_ZEROINIT,
        }
    }
}
