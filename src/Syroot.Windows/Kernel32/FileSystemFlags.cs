﻿using System;

namespace Syroot.Windows
{
    public static partial class Kernel32
    {
        /// <summary>
        /// Represents supported features of a file system.
        /// </summary>
        [Flags]
        public enum FileSystemFlags : uint
        {
            /// <summary>The specified volume supports case-sensitive file names.</summary>
            FILE_CASE_SENSITIVE_SEARCH = 1 << 0,
            /// <summary>The specified volume supports preserved case of file names when it places a name on disk.</summary>
            FILE_CASE_PRESERVED_NAMES = 1 << 1,
            /// <summary>The specified volume supports Unicode in file names as they appear on disk.</summary>
            FILE_UNICODE_ON_DISK = 1 << 2,
            /// <summary>The specified volume preserves and enforces access control lists (ACL).</summary>
            FILE_PERSISTENT_ACLS = 1 << 3,
            /// <summary>The specified volume supports file-based compression.</summary>
            FILE_FILE_COMPRESSION = 1 << 4,
            /// <summary>The specified volume supports disk quotas.</summary>
            FILE_VOLUME_QUOTAS = 1 << 5,
            /// <summary>The specified volume supports sparse files.</summary>
            FILE_SUPPORTS_SPARSE_FILES = 1 << 6,
            /// <summary>The specified volume supports reparse points.</summary>
            FILE_SUPPORTS_REPARSE_POINTS = 1 << 7,
            FILE_SUPPORTS_REMOTE_STORAGE = 1 << 8,
            FILE_RETURNS_CLEANUP_RESULT_INFO = 1 << 9,
            FILE_SUPPORTS_POSIX_UNLINK_RENAME = 1 << 10,
            /// <summary>The specified volume is a compressed volume, for example, a DoubleSpace volume.</summary>
            FILE_VOLUME_IS_COMPRESSED = 1 << 15,
            /// <summary>The specified volume supports object identifiers.</summary>
            FILE_SUPPORTS_OBJECT_IDS = 1 << 16,
            /// <summary>The specified volume supports the Encrypted File System (EFS).</summary>
            FILE_SUPPORTS_ENCRYPTION = 1 << 17,
            /// <summary>The specified volume supports named streams.</summary>
            FILE_NAMED_STREAMS = 1 << 18,
            /// <summary>The specified volume is read-only.</summary>
            FILE_READ_ONLY_VOLUME = 1 << 19,
            /// <summary>The specified volume supports a single sequential write.</summary>
            FILE_SEQUENTIAL_WRITE_ONCE = 1 << 20,
            /// <summary>The specified volume supports transactions.</summary>
            FILE_SUPPORTS_TRANSACTIONS = 1 << 21,
            /// <summary>The specified volume supports hard links.</summary>
            FILE_SUPPORTS_HARD_LINKS = 1 << 22,
            /// <summary>The specified volume supports extended attributes.</summary>
            FILE_SUPPORTS_EXTENDED_ATTRIBUTES = 1 << 23,
            /// <summary>The file system supports open by FileID.</summary>
            FILE_SUPPORTS_OPEN_BY_FILE_ID = 1 << 24,
            /// <summary>The specified volume supports update sequence number (USN) journals.</summary>
            FILE_SUPPORTS_USN_JOURNAL = 1 << 25,
            FILE_SUPPORTS_INTEGRITY_STREAMS = 1 << 26,
            FILE_SUPPORTS_BLOCK_REFCOUNTING = 1 << 27,
            FILE_SUPPORTS_SPARSE_VDL = 1 << 28,
            /// <summary>The specified volume is a direct access (DAX) volume.</summary>
            FILE_DAX_VOLUME = 1 << 29,
            FILE_SUPPORTS_GHOSTING = 1 << 30
        }
    }
}
