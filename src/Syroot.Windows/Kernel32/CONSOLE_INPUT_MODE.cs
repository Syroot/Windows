﻿using System;

namespace Syroot.Windows
{
    public static partial class Kernel32
    {
        [Flags]
        public enum ConsoleInputMode : uint
        {
            ENABLE_PROCESSED_INPUT = 1 << 0,
            ENABLE_LINE_INPUT = 1 << 1,
            ENABLE_ECHO_INPUT = 1 << 2,
            ENABLE_WINDOW_INPUT = 1 << 3,
            ENABLE_MOUSE_INPUT = 1 << 4,
            ENABLE_INSERT_MODE = 1 << 5,
            ENABLE_QUICK_EDIT_MODE = 1 << 6,
            ENABLE_EXTENDED_FLAGS = 1 << 7,
            ENABLE_AUTO_POSITION = 1 << 8,
            ENABLE_VIRTUAL_TERMINAL_INPUT = 1 << 10
        }
    }
}
