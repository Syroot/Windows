﻿using System;

namespace Syroot.Windows
{
    public static partial class Kernel32
    {
        [Flags]
        public enum ConsoleOutputMode : uint
        {
            ENABLE_PROCESSED_OUTPUT = 1 << 0,
            ENABLE_WRAP_AT_EOL_OUTPUT = 1 << 1,
            ENABLE_VIRTUAL_TERMINAL_PROCESSING = 1 << 2,
            DISABLE_NEWLINE_AUTO_RETURN = 1 << 3,
            ENABLE_LVB_GRID_WORLDWIDE = 1 << 4
        }
    }
}
