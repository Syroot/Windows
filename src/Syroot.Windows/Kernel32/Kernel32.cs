﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace Syroot.Windows
{
    /// <summary>
    /// Represents definitions in the Windows Kernel32 library as part of the WinAPI.
    /// </summary>
    public static partial class Kernel32
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        /// <summary>Maximum length of a path.</summary>
        public const uint MAX_PATH = 260;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Frees the loaded dynamic-link library (DLL) module and, if necessary, decrements its reference count. When
        /// the reference count reaches zero, the module is unloaded from the address space of the calling process and
        /// the handle is no longer valid.
        /// </summary>
        /// <param name="hLibModule">A handle to the loaded library module. The LoadLibrary, LoadLibraryEx,
        /// GetModuleHandle, or GetModuleHandleEx function returns this handle.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(Kernel32), SetLastError = true)]
        public static extern bool FreeLibrary(HMODULE hLibModule);

        [DllImport(nameof(Kernel32))]
        public static extern bool GetConsoleMode(IntPtr hConsoleHandle, out ConsoleInputMode lpMode);

        [DllImport(nameof(Kernel32))]
        public static extern bool GetConsoleMode(IntPtr hConsoleHandle, out ConsoleOutputMode lpMode);

        [DllImport(nameof(Kernel32), SetLastError = true)]
        public static extern bool GetFileInformationByHandle(HANDLE hFile,
            ref BY_HANDLE_FILE_INFORMATION lpFileInformation);

        /// <summary>
        /// Retrieves a module handle for the specified module.
        /// </summary>
        /// <param name="lpModuleName">The name of the loaded module (either a .dll or .exe file).</param>
        /// <returns>If the function succeeds, the return value is a handle to the specified module.</returns>
        [DllImport(nameof(Kernel32), CharSet = CharSet.Auto, SetLastError = true)]
        public static extern HMODULE GetModuleHandle(string lpModuleName);

        /// <summary>
        /// Retrieves the address of an exported function or variable from the specified dynamic-link library (DLL).
        /// </summary>
        /// <param name="hModule">A handle to the DLL module that contains the function or variable.</param>
        /// <param name="lpProcName">The function or variable name, or the function's ordinal value.</param>
        /// <returns>If the function succeeds, the return value is the address of the exported function or variable.</returns>
        [DllImport(nameof(Kernel32), SetLastError = true)]
        public static extern FARPROC GetProcAddress(HMODULE hModule, string lpProcName);

        [DllImport(nameof(Kernel32), SetLastError = true)]
        public static extern IntPtr GetStdHandle(StandardHandle nStdHandle);

        /// <summary>
        /// Retrieves information about the file system and volume associated with the specified root directory.
        /// </summary>
        /// <param name="lpRootPathName">A pointer to a string that contains the root directory of the volume to be
        /// described. If this parameter is <see langword="null"/>, the root of the current directory is used. A
        /// trailing backslash is required.</param>
        /// <param name="lpVolumeNameBuffer">A pointer to a buffer that receives the name of a specified volume.</param>
        /// <param name="nVolumeNameSize">The length of a volume name buffer.</param>
        /// <param name="lpVolumeSerialNumber">A pointer to a variable that receives the volume serial number.</param>
        /// <param name="lpMaximumComponentLength">A pointer to a variable that receives the maximum length.</param>
        /// <param name="lpFileSystemFlags">A pointer to a variable that receives flags associated with the specified
        /// file system.</param>
        /// <param name="lpFileSystemNameBuffer">A pointer to a buffer that receives the name of the file system, for
        /// example, the FAT file system or the NTFS file system.</param>
        /// <param name="nFileSystemNameSize">The length of the file system name buffer.</param>
        /// <returns>If all the requested information is retrieved, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(Kernel32), CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool GetVolumeInformation(string lpRootPathName, StringBuilder lpVolumeNameBuffer,
            uint nVolumeNameSize, out uint lpVolumeSerialNumber, out uint lpMaximumComponentLength,
            out FileSystemFlags lpFileSystemFlags, StringBuilder lpFileSystemNameBuffer, uint nFileSystemNameSize);

        /// <summary>
        /// Allocates the specified number of bytes from the heap.
        /// </summary>
        /// <param name="uFlags">The memory allocation attributes.</param>
        /// <param name="dwBytes">The number of bytes to allocate.</param>
        /// <returns>If the function succeeds, the return value is a handle to the newly allocated memory object.</returns>
        [DllImport(nameof(Kernel32), SetLastError = true)]
        public static extern HGLOBAL GlobalAlloc(GlobalAllocFlags uFlags, uint dwBytes);

        /// <summary>
        /// rees the specified global memory object and invalidates its handle.
        /// </summary>
        /// <param name="hMem">A handle to the global memory object.</param>
        /// <returns>If the function succeeds, the return value is NULL.</returns>
        [DllImport(nameof(Kernel32), SetLastError = true)]
        public static extern HGLOBAL GlobalFree(HGLOBAL hMem);

        /// <summary>
        /// Locks a global memory object and returns a pointer to the first byte of the object's memory block.
        /// </summary>
        /// <param name="hMem">A handle to the global memory object.</param>
        /// <returns>If the function succeeds, the return value is a pointer to the first byte of the memory block.</returns>
        [DllImport(nameof(Kernel32), SetLastError = true)]
        public static extern IntPtr GlobalLock(HGLOBAL hMem);

        /// <summary>
        /// Decrements the lock count associated with a memory object that was allocated with
        /// <see cref="GlobalAllocFlags.GMEM_MOVEABLE"/>. This function has no effect on memory objects allocated with
        /// <see cref="GlobalAllocFlags.GMEM_FIXED"/>.
        /// </summary>
        /// <param name="hMem">A handle to the global memory object.</param>
        /// <returns>If the memory object is still locked after decrementing the lock count, the return value is
        /// <see langword="true"/>.</returns>
        [DllImport(nameof(Kernel32), SetLastError = true)]
        public static extern bool GlobalUnlock(HGLOBAL hMem);

        /// <summary>
        /// Loads the specified module into the address space of the calling process. The specified module may cause
        /// other modules to be loaded.
        /// </summary>
        /// <param name="lpLibFileName">The name of the module. This can be either a library module (a .dll file) or an
        /// executable module (an .exe file).</param>
        /// <returns>If the function succeeds, the return value is a handle to the module.</returns>
        [DllImport(nameof(Kernel32), SetLastError = true)]
        public static extern HMODULE LoadLibrary(string lpLibFileName);

        [DllImport(nameof(Kernel32))]
        public static extern bool SetConsoleMode(IntPtr hConsoleHandle, ConsoleInputMode dwMode);

        [DllImport(nameof(Kernel32))]
        public static extern bool SetConsoleMode(IntPtr hConsoleHandle, ConsoleOutputMode dwMode);
    }
}
