﻿namespace Syroot.Windows
{
    public static partial class Kernel32
    {
        public enum StandardHandle : uint
        {
            STD_ERROR_HANDLE = unchecked((uint)-12),
            STD_OUTPUT_HANDLE = unchecked((uint)-11),
            STD_INPUT_HANDLE = unchecked((uint)-10)
        }
    }
}
