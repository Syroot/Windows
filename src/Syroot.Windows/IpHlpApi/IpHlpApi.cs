﻿using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    /// <summary>
    /// Represents definitions in the Windows IpHlpApi library as part of the WinAPI.
    /// </summary>
    public static class IpHlpApi
    {
        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Retrieves the index of the interface that has the best route to the specified IPv4 address.
        /// </summary>
        /// <param name="dwDestAddr">The destination IPv4 address for which to retrieve the interface that has the best
        /// route, in the form of an IPAddr structure.</param>
        /// <param name="pdwBestIfIndex">A pointer to a DWORD variable that receives the index of the interface that has
        /// the best route to the IPv4 address specified by dwDestAddr.</param>
        /// <returns>If the function succeeds, the return value is <see cref="Error.ERROR_SUCCESS"/>.</returns>
        [DllImport(nameof(IpHlpApi))]
        public static extern Error GetBestInterface(uint dwDestAddr, ref uint pdwBestIfIndex);
    }
}
