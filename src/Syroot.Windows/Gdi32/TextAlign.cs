﻿using System;

namespace Syroot.Windows
{
    public static partial class Gdi32
    {
        /// <summary>
        /// Represents text alignments. Only one flag can be chosen from those that affect horizontal and vertical
        /// alignment. In addition, only one of the two flags that alter the current position can be chosen.
        /// </summary>
        public enum TextAlign : int
        {
            TA_NOUPDATECP = 0,
            /// <summary>The reference point will be on the left edge of the bounding rectangle.</summary>
            TA_LEFT = 0,
            /// <summary>The reference point will be on the top edge of the bounding rectangle.</summary>
            TA_TOP = 0,
            /// <summary>The current position is updated after each text output call. The current position is used as
            /// the reference point.</summary>
            TA_UPDATECP = 1,
            /// <summary>The reference point will be on the right edge of the bounding rectangle.</summary>
            TA_RIGHT = 2,
            /// <summary>The reference point will be aligned horizontally with the center of the bounding rectangle.</summary>
            TA_CENTER = 6,
            /// <summary>The reference point will be on the bottom edge of the bounding rectangle.</summary>
            TA_BOTTOM = 8,
            /// <summary>The reference point will be on the base line of the text.</summary>
            TA_BASELINE = 24,
            /// <summary>The text is laid out in right to left reading order, as opposed to the default left to right
            /// order.</summary>
            TA_RTLREADING = 256,
            TA_MASK = TA_BASELINE + TA_CENTER + TA_UPDATECP + TA_RTLREADING
        }
    }
}
