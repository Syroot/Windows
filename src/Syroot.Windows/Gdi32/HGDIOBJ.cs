﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    public static partial class Gdi32
    {
        /// <summary>
        /// Represents a GDI object handle.
        /// </summary>
        [DebuggerDisplay("{_pointer,h}")]
        [StructLayout(LayoutKind.Sequential)]
        public struct HGDIOBJ
        {
            // ---- FIELDS ---------------------------------------------------------------------------------------------

            private readonly IntPtr _pointer;

            // ---- CONSTRUCTORS & DESTRUCTOR --------------------------------------------------------------------------

            public HGDIOBJ(IntPtr pointer) => _pointer = pointer;

            // ---- OPERATORS ------------------------------------------------------------------------------------------

            public static implicit operator IntPtr(HGDIOBJ handle) => handle._pointer;

            public static implicit operator HGDIOBJ(IntPtr pointer) => new HGDIOBJ(pointer);
        }
    }
}
