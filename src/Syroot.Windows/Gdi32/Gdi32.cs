﻿using System;
using System.Runtime.InteropServices;
using static Syroot.Windows.User32;

namespace Syroot.Windows
{
    /// <summary>
    /// Represents definitions in the Windows Gdi32 library as part of the WinAPI.
    /// </summary>
    public static partial class Gdi32
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        /// <summary>Value indicating an invalid color.</summary>
        public const uint CLR_INVALID = UInt32.MaxValue;
        /// <summary>Value indicating a generic GDI error.</summary>
        public const uint GDI_ERROR = UInt32.MaxValue;

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a memory device context (DC) compatible with the specified device.
        /// </summary>
        /// <param name="hdc">A handle to an existing DC.</param>
        /// <returns>If the function succeeds, the return value is the handle to a memory DC.</returns>
        [DllImport(nameof(Gdi32))]
        public static extern HDC CreateCompatibleDC(HDC hdc);

        /// <summary>
        /// Draws text using the currently selected font, background color, and text color. You can optionally provide
        /// dimensions to be used for clipping, opaquing, or both.
        /// </summary>
        /// <param name="hdc">A handle to the device context.</param>
        /// <param name="x">The x-coordinate, in logical coordinates, of the reference point used to position the
        /// string.</param>
        /// <param name="y">The y-coordinate, in logical coordinates, of the reference point used to position the
        /// string.</param>
        /// <param name="options">Specifies how to use the application-defined rectangle. This parameter can be one or
        /// more of <see cref="ExtTextOptions"/>.</param>
        /// <param name="lprect">A pointer to an optional <see cref="RECT"/> structure that specifies the dimensions, in
        /// logical coordinates, of a rectangle that is used for clipping, opaquing, or both.</param>
        /// <param name="lpString">A pointer to a string that specifies the text to be drawn.</param>
        /// <param name="c">The length of the string pointed to by <paramref name="lpString"/>.</param>
        /// <param name="lpDx">A pointer to an optional array of values that indicate the distance between origins of
        /// adjacent character cells.</param>
        /// <returns>If the string is drawn, the return value is <see langword="true"/>. If the function fails, the
        /// return value is <see langword="false"/>.</returns>
        [DllImport(nameof(Gdi32))]
        public static extern bool ExtTextOut(HDC hdc, int x, int y, ExtTextOptions options, ref RECT lprect,
            string lpString, uint c, IntPtr lpDx);

        /// <summary>
        /// Flushes the calling thread's current batch.
        /// </summary>
        /// <returns>If all functions in the current batch succeed, the return value is <see langword="true"/>. If not
        /// all functions in the current batch succeed, the return value is <see langword="false"/>, indicating that at
        /// least one function returned an error.</returns>
        [DllImport(nameof(Gdi32))]
        public static extern bool GdiFlush();

        /// <summary>
        /// Retrieves the widths, in logical units, of consecutive characters in a specified range from the current
        /// TrueType font. This function succeeds only with TrueType fonts.
        /// </summary>
        /// <param name="hdc">A handle to the device context.</param>
        /// <param name="wFirst">The first character in the group of consecutive characters from the current font.</param>
        /// <param name="wLast">The last character in the group of consecutive characters from the current font.</param>
        /// <param name="lpABC">A pointer to an array of ABC structures that receives the character widths, in logical
        /// units. This array must contain at least as many ABC structures as there are characters in the range
        /// specified by the <paramref name="wFirst"/> and <paramref name="wLast"/> parameters.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(Gdi32), CharSet = CharSet.Auto)]
        public static extern bool GetCharABCWidths(HDC hdc, uint wFirst, uint wLast, ref ABC lpABC);

        /// <summary>
        /// Retrieves the red, green, blue (RGB) color value of the pixel at the specified coordinates.
        /// </summary>
        /// <param name="hdc">A handle to the device context.</param>
        /// <param name="x">The x-coordinate, in logical units, of the pixel to be examined.</param>
        /// <param name="y">The y-coordinate, in logical units, of the pixel to be examined.</param>
        /// <returns>The return value specifies the RGB of the pixel.</returns>
        [DllImport(nameof(Gdi32), CharSet = CharSet.Auto)]
        public static extern COLORREF GetPixel(HDC hdc, int x, int y);

        /// <summary>
        /// Retrieves a handle to one of the stock pens, brushes, fonts, or palettes.
        /// </summary>
        /// <param name="i">The type of stock object.</param>
        /// <returns></returns>
        [DllImport(nameof(Gdi32))]
        public static extern HGDIOBJ GetStockObject(StockObject i);

        /// <summary>
        /// Fills the specified buffer with the metrics for the currently selected font.
        /// </summary>
        /// <param name="hdc">A handle to the device context.</param>
        /// <param name="lptm">A pointer to the <see cref="TEXTMETRIC"/> structure that receives the text metrics.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(Gdi32), CharSet = CharSet.Auto)]
        public static extern bool GetTextMetrics(HDC hdc, out TEXTMETRIC lptm);
        /// <summary>
        /// Fills the specified buffer with the metrics for the currently selected font.
        /// </summary>
        /// <param name="hdc">A handle to the device context.</param>
        /// <param name="lptm">A pointer to the <see cref="TEXTMETRICA"/> structure that receives the text metrics.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(Gdi32), CharSet = CharSet.Ansi)]
        public static extern bool GetTextMetrics(HDC hdc, out TEXTMETRICA lptm);
        /// <summary>
        /// Fills the specified buffer with the metrics for the currently selected font.
        /// </summary>
        /// <param name="hdc">A handle to the device context.</param>
        /// <param name="lptm">A pointer to the <see cref="TEXTMETRICW"/> structure that receives the text metrics.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>.</returns>
        [DllImport(nameof(Gdi32), CharSet = CharSet.Unicode)]
        public static extern bool GetTextMetrics(HDC hdc, out TEXTMETRICW lptm);

        /// <summary>
        /// Selects a red, green, blue (RGB) color based on the arguments supplied and the color capabilities of the
        /// output device.
        /// </summary>
        /// <param name="r">The intensity of the red color.</param>
        /// <param name="g">The intensity of the green color.</param>
        /// <param name="b">The intensity of the blue color.</param>
        /// <returns>The resulting <see cref="COLORREF"/> value.</returns>
        public static COLORREF RGB(byte r, byte g, byte b) => r | g << 8 | b << 16;

        /// <summary>
        /// Selects an object into the specified device context (DC). The new object replaces the previous object of the
        /// same type.
        /// </summary>
        /// <param name="hdc">A handle to the DC.</param>
        /// <param name="h">A handle to the object to be selected.</param>
        /// <returns>If the selected object is not a region and the function succeeds, the return value is a handle to
        /// the object being replaced.</returns>
        [DllImport(nameof(Gdi32))]
        public static extern HGDIOBJ SelectObject(HDC hdc, HGDIOBJ h);

        /// <summary>
        /// Sets the current background color to the specified color value, or to the nearest physical color if the
        /// device cannot represent the specified color value.
        /// </summary>
        /// <param name="hdc">A handle to the device context.</param>
        /// <param name="color">The new background color.</param>
        /// <returns>If the function succeeds, the return value specifies the previous background color. If the function
        /// fails, the return value is <see cref="CLR_INVALID"/>.</returns>
        [DllImport(nameof(Gdi32))]
        public static extern COLORREF SetBkColor(HDC hdc, COLORREF color);

        /// <summary>
        /// Sets the background mix mode of the specified device context. The background mix mode is used with text,
        /// hatched brushes, and pen styles that are not solid lines.
        /// </summary>
        /// <param name="hdc">A handle to the device context.</param>
        /// <param name="mode">The background mode.</param>
        /// <returns>If the function succeeds, the return value specifies the previous background mode.
        /// If the function fails, the return value is <c>0</c>.</returns>
        [DllImport(nameof(Gdi32))]
        public static extern BkMode SetBkMode(HDC hdc, BkMode mode);

        /// <summary>
        /// The SetTextAlign function sets the text-alignment flags for the specified device context.
        /// </summary>
        /// <param name="hdc">A handle to the device context.</param>
        /// <param name="align">The text alignment by using a mask of the values in <see cref="TextAlign"/>.</param>
        /// <returns>If the function succeeds, the return value is the previous-text-alignment setting. If the function
        /// fails, the return value is <see cref="GDI_ERROR"/>.</returns>
        [DllImport(nameof(Gdi32))]
        public static extern uint SetTextAlign(HDC hdc, TextAlign align);

        /// <summary>
        /// Sets the text color for the specified device context to the specified color.
        /// </summary>
        /// <param name="hdc">A handle to the device context.</param>
        /// <param name="color">The color of the text.</param>
        /// <returns>If the function succeeds, the return value is a color reference for the previous text color. If the
        /// function fails, the return value is <see cref="CLR_INVALID"/>.</returns>
        [DllImport(nameof(Gdi32))]
        public static extern COLORREF SetTextColor(HDC hdc, COLORREF color);

        /// <summary>
        /// Writes a character string at the specified location, using the currently selected font, background color,
        /// and text color.
        /// </summary>
        /// <param name="hdc">A handle to the device context.</param>
        /// <param name="x">The x-coordinate, in logical coordinates, of the reference point that the system uses to
        /// align the string.</param>
        /// <param name="y">The y-coordinate, in logical coordinates, of the reference point that the system uses to
        /// align the string.</param>
        /// <param name="lpString">A pointer to the string to be drawn.</param>
        /// <param name="c">The length of the string pointed to by <paramref name="lpString"/>, in characters.</param>
        /// <returns>If the function succeeds, the return value is <see langword="true"/>. If the function fails, the
        /// return value is <see langword="false"/>.</returns>
        [DllImport(nameof(Gdi32), CharSet = CharSet.Auto)]
        public static extern bool TextOut(HDC hdc, int x, int y, string lpString, int c);
    }
}
