﻿using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    public static partial class Gdi32
    {
        /// <summary>
        /// Contains the width of a character in a TrueType font.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct ABC
        {
            // ---- FIELDS ---------------------------------------------------------------------------------------------

            /// <summary>The A spacing of the character. The A spacing is the distance to add to the current position
            /// before drawing the character glyph.</summary>
            public int abcA;
            /// <summary>The B spacing of the character. The B spacing is the width of the drawn portion of the
            /// character glyph.</summary>
            public uint abcB;
            /// <summary>The C spacing of the character. The C spacing is the distance to add to the current position to
            /// provide white space to the right of the character glyph.</summary>
            public int abcC;
        }
    }
}
