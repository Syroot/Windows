﻿using System;

namespace Syroot.Windows
{
    public static partial class Gdi32
    {
        /// <summary>
        /// Represents how to use the application-defined rectangle.
        /// </summary>
        [Flags]
        public enum ExtTextOptions : int
        {
            /// <summary>The current background color should be used to fill the rectangle.</summary>
            ETO_OPAQUE = 1 << 1,
            /// <summary>The text will be clipped to the rectangle.</summary>
            ETO_CLIPPED = 1 << 2,
            /// <summary>The lpString array refers to an array returned from GetCharacterPlacement and should be parsed
            /// directly by GDI as no further language-specific processing is required.</summary>
            ETO_GLYPH_INDEX = 1 << 4,
            /// <summary>If this value is specified and a Hebrew or Arabic font is selected into the device context, the 
            /// string is output using right-to-left reading order.</summary>
            ETO_RTLREADING = 1 << 7,
            /// <summary>To display numbers, use digits appropriate to the locale.</summary>
            ETO_NUMERICSLOCAL = 1 << 10,
            /// <summary>To display numbers, use European digits.</summary>
            ETO_NUMERICSLATIN = 1 << 11,
            /// <summary>Reserved for system use. If an application sets this flag, it loses international scripting
            /// support and in some cases it may display no text at all.</summary>
            ETO_IGNORELANGUAGE = 1 << 12,
            /// <summary>When this is set, the array pointed to by lpDx contains pairs of values. The first value of
            /// each pair is, as usual, the distance between origins of adjacent character cells, but the second value
            /// is the displacement along the vertical direction of the font.</summary>
            ETO_PDY = 1 << 13,
            ETO_REVERSE_INDEX_MAP = 1 << 16
        }
    }
}
