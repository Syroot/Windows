﻿using System;
using System.Drawing;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    public static partial class Gdi32
    {
        /// <summary>
        /// Represents an RGB color.
        /// </summary>
        [StructLayout(LayoutKind.Sequential)]
        public struct COLORREF
        {
            // ---- FIELDS ---------------------------------------------------------------------------------------------

            /// <summary>The RGBA value.</summary>
            public int Value;

            // ---- OPERATORS ------------------------------------------------------------------------------------------

            /// <summary>
            /// Implicitly converts a <see cref="Color"/> to a <see cref="COLORREF"/> instance.
            /// </summary>
            /// <param name="color">The <see cref="Color"/> instance to convert.</param>
            public static implicit operator COLORREF(Color color) => new COLORREF { Value = color.ToArgb() };

            /// <summary>
            /// Implicitly converts a <see cref="COLORREF"/> to a <see cref="Color"/> instance.
            /// </summary>
            /// <param name="colorref">The <see cref="COLORREF"/> instance to convert.</param>
            public static implicit operator Color(COLORREF colorref) => Color.FromArgb(colorref.Value);

            /// <summary>
            /// Implicitly converts an <see cref="Int32"/> to a <see cref="COLORREF"/> instance.
            /// </summary>
            /// <param name="value">The <see cref="Int32"/> instance to convert.</param>
            public static implicit operator COLORREF(int value) => new COLORREF { Value = value };

            /// <summary>
            /// Implicitly converts a <see cref="COLORREF"/> to an <see cref="Int32"/> instance.
            /// </summary>
            /// <param name="colorref">The <see cref="COLORREF"/> instance to convert.</param>
            public static implicit operator int(COLORREF colorref) => colorref.Value;

            /// <summary>
            /// Implicitly converts an <see cref="UInt32"/> to a <see cref="COLORREF"/> instance.
            /// </summary>
            /// <param name="value">The <see cref="UInt32"/> instance to convert.</param>
            public static implicit operator COLORREF(uint value) => new COLORREF { Value = unchecked((int)value) };

            /// <summary>
            /// Implicitly converts a <see cref="COLORREF"/> to an <see cref="UInt32"/> instance.
            /// </summary>
            /// <param name="colorref">The <see cref="COLORREF"/> instance to convert.</param>
            public static implicit operator uint(COLORREF colorref) => unchecked((uint)colorref.Value);
        }
    }
}
