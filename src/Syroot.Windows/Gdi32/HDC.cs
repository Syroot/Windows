﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    public static partial class Gdi32
    {
        /// <summary>
        /// Represents a graphics device context.
        /// </summary>
        [DebuggerDisplay("{_pointer,h}")]
        [StructLayout(LayoutKind.Sequential)]
        public struct HDC
        {
            // ---- FIELDS ---------------------------------------------------------------------------------------------

            private readonly IntPtr _pointer;

            // ---- CONSTRUCTORS & DESTRUCTOR --------------------------------------------------------------------------

            public HDC(IntPtr pointer) => _pointer = pointer;

            // ---- OPERATORS ------------------------------------------------------------------------------------------

            public static implicit operator IntPtr(HDC handle) => handle._pointer;

            public static implicit operator HDC(IntPtr pointer) => new HDC(pointer);
        }
    }
}
