﻿namespace Syroot.Windows
{
    public static partial class Gdi32
    {
        public enum BkMode : int
        {
            /// <summary>Background remains untouched.</summary>
            TRANSPARENT = 1,
            /// <summary>Background is filled with the current background color before the text, hatched brush, or pen
            /// is drawn.</summary>
            OPAQUE = 2
        }
    }
}
