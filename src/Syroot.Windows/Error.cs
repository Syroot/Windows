﻿namespace Syroot.Windows
{
    /// <summary>
    /// Represents Win32 system error codes.
    /// </summary>
    public enum Error
    {
        /// <summary>The operation completed successfully.</summary>
        ERROR_SUCCESS
    }
}
