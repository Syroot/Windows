﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    [DebuggerDisplay("{_pointer,h}")]
    [StructLayout(LayoutKind.Sequential)]
    public struct HWND
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        /// <summary>Places the window at the bottom of the Z order.</summary>
        public static readonly HWND HWND_BOTTOM = new HWND(new IntPtr(1));
        /// <summary>Places the window above all non-topmost windows.</summary>
        public static readonly HWND HWND_NOTOPMOST = new HWND(new IntPtr(-2));
        /// <summary>Places the window at the top of the Z order.</summary>
        public static readonly HWND HWND_TOP = new HWND(new IntPtr(0));
        /// <summary>Places the window above all non-topmost windows.</summary>
        public static readonly HWND HWND_TOPMOST = new HWND(new IntPtr(-1));

        private readonly IntPtr _pointer;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public HWND(IntPtr pointer) => _pointer = pointer;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static implicit operator IntPtr(HWND handle) => handle._pointer;

        public static implicit operator HWND(IntPtr pointer) => new HWND(pointer);
    }
}
