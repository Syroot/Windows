﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    [DebuggerDisplay("{_pointer,h}")]
    [StructLayout(LayoutKind.Sequential)]
    public struct HMENU
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly IntPtr _pointer;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public HMENU(IntPtr pointer) => _pointer = pointer;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static implicit operator IntPtr(HMENU handle) => handle._pointer;

        public static implicit operator HMENU(IntPtr pointer) => new HMENU(pointer);
    }
}
