﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    [DebuggerDisplay("{_pointer,h}")]
    [StructLayout(LayoutKind.Sequential)]
    public struct HBRUSH
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly IntPtr _pointer;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public HBRUSH(IntPtr pointer) => _pointer = pointer;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static implicit operator IntPtr(HBRUSH handle) => handle._pointer;

        public static implicit operator HBRUSH(IntPtr pointer) => new HBRUSH(pointer);
    }
}
