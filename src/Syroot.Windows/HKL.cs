﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace Syroot.Windows
{
    /// <summary>
    /// A language identifier is a standard international numeric abbreviation for the language in a country or
    /// geographical region. Each language has a unique language identifier (data type LANGID), a 16-bit value that
    /// consists of a primary language identifier and a sublanguage identifier. For details of language identifiers,
    /// see Language Identifier Constants and Strings.
    /// </summary>
    [DebuggerDisplay("{_pointer,h}")]
    [StructLayout(LayoutKind.Sequential)]
    public struct HKL
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private readonly IntPtr _pointer;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        public HKL(IntPtr pointer) => _pointer = pointer;

        // ---- OPERATORS ----------------------------------------------------------------------------------------------

        public static implicit operator IntPtr(HKL handle) => handle._pointer;

        public static implicit operator HKL(IntPtr pointer) => new HKL(pointer);
    }
}
