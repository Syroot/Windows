_This project has been retired in favor of the official [C#/Win32](https://github.com/microsoft/CsWin32) project._

# Windows

This repository provides .NET definitions (p/invoke signatures, structs, enums) for many common, but also several exotic
Win32 APIs. It is used by other Syroot projects and extended as needed, which means that the surface of this library may
change heavily between releases.

This library is built with the following principles kept in mind:

## Simplified porting of native code

WinAPI header files are represented as static classes, and importing those statically allows direct access to all of the
definitions in such files, yielding code similar to the native representation.

To prevent polluting the available symbols too much, #define values are wrapped inside enums with a fitting name - you
can additionally import those statically too if you prefer to omit their names.

Parameter and field names are not changed to match C# guidelines, their original casing and prefixes are kept.

## Strongly-type handle values

Values of handles are strongly typed into implicitly `IntPtr`-convertible structs like `HWND` or `HMODULE`.
